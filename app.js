/*
    This file is generated and updated by Sencha Cmd. You can edit this file as
    needed for your application, but these edits will have to be merged by
    Sencha Cmd when it performs code generation tasks such as generating new
    models, controllers or views and when running "sencha app upgrade".

    Ideally changes to this file would be limited and most work would be done
    in other places (such as Controllers). If Sencha Cmd cannot merge your
    changes and its generated code, it will produce a "merge conflict" that you
    will need to resolve manually.
*/

Ext.application({
    name: 'Ksiazece',

    requires: [
        'Ext.MessageBox',
        'Ksiazece.Config'
    ],

    controllers: [
        'Main', 'Account', 'Episodes', 'Ranking', 'Specials', 'Generator', 'Autocomplete', 'Profile', 'Push', 'Share', 'ResetPassword'
    ],

    models: ['Episode', 'City', 'Message', 'Mem'],

    stores: ['Episodes', 'Cities', 'Messages'],

    views: [
        'Main',
        'Tabs',
        'account.Login', 'account.Remind', 'account.Register', 'account.UserData', 'account.Terms', 'account.Profile', 'account.User', 'account.UserMessages',
        'episode.List', 'episode.Episode', 'episode.Result',
        'specials.Specials', 'specials.Message', 'specials.ThankYou', 'specials.Joke', 'specials.Photo', 'specials.quiz.Quiz', 'specials.quiz.Text', 'specials.Results',
        'ranking.Ranking',
        'generator.Generator', 'generator.Choose', 'generator.Edit', 'generator.Preview', 'generator.List'
    ],

    launch: function() {
        var main = Ext.create('Ksiazece.view.Main');
        // Destroy the #appLoadingIndicator element
        Ext.fly('appLoadingIndicator').destroy();

        if(typeof StatusBar !== 'undefined'){
            StatusBar.overlaysWebView(false);
        }

        if(typeof FB != 'undefined') {
            FB.init({
                appId: "1414726162144056",
                nativeInterface: CDV.FB,
                useCachedDialogs: false
            });
        }

        Ext.Date.dayNames = ['Niedziela', 'Poniedziałek', 'Wtorek', 'Środa', 'Czwartek', 'Piątek', 'Sobota'];
        Ext.Date.monthNames = ['Styczeń', 'Luty', 'Marzec', 'Kwiecień', 'Maj', 'Czerwiec', 'Lipiec', 'Sierpień', 'Wrzesień', 'Październik', 'Listopad', 'Grudzień'];

        // Initialize the main view
        Ext.Viewport.add(main);

        if (localStorage.getItem("logged") == 'true') {
            Ksiazece.app.getController('Main').initTabs(false);
        }

        this.getApplication().getHistory().add(Ext.create('Ext.app.Action', {
            url: 'episodes'
        }));

        if (Ext.os.is.Android) {
            Ksiazece.app.getController('Push').initAndroidPushwoosh();

            //hack na niepokazywanie tabbara przydockowanego na dole
            //document.body.style.marginTop = "20px";
            //Ext.Viewport.setHeight(Ext.Viewport.getWindowHeight() - 20);
        }
        else if(Ext.os.is.iOS) {
            Ksiazece.app.getController('Push').initIOSPushwoosh();
            app.receivedEvent('deviceready');
        }
        else if(Ext.os.is.Windows) {
            Ksiazece.app.getController('Push').initWindowsPushwoosh();
        }

        document.addEventListener("backbutton", function(){
            Ksiazece.app.getController('Main').backButtonTapped();
        }, false);
    }
});

//<debug>
function tapBackbutton(){
    var event = document.createEvent("HTMLEvents");
    event.initEvent("backbutton",true,false);

    document.dispatchEvent(event);
}
//</debug>
