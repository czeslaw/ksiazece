#!/bin/bash

REPO_DIR=~/PhpstormProjects/App_Barmanska/;
DESTINATION_DIR=~/build_apps/;

echo
echo "Książęce app builder"
echo "-------------------------------"
echo 
echo 
cd $REPO_DIR;

printf "\t- sencha compiling...\n\n\n";
sencha app build package;

printf "\t- compressing package...\n\n\n";
cd build/package;
zip -r Ksiazece.zip Ksiazece;

printf "\t- moving package to %s \n\n\n" $DESTINATION_DIR;
mv Ksiazece.zip ${DESTINATION_DIR}/Ksiazece-${1}.zip;

printf "\n\nDone %s \n\n" ${DESTINATION_DIR}/Ksiazece-${1}.zip;