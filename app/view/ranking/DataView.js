Ext.define('Ksiazece.view.ranking.DataView', {
    extend: 'Ext.Panel',

    xtype: 'rankingdataview',

    requires: [
        'Ksiazece.extension.DataViewPaging',
        'Ext.dataview.DataView'
    ],

    config: {
        url: '',
        message: {},
        cls: 'ranking',
        layout: 'vbox',
        items: [
            {
                xtype: 'message',
                text: 'W tym miesiącu gramy o bilet na koncert <b>Coldplay</b>'
            },
            {
                cls: 'ranking-header',
                html: ['<div class="cell" style="width: 8em;">L.p.</div>',
                    '<div class="cell">Imię i Nazwisko</div>',
                    '<div class="cell" style="width: 25%;">Pkt</div>'].join('')
            },
            {
                flex: 1,
                xtype: 'list',
                loadingText: 'Ładuję...',
                disableSelection: true,
                itemTpl: [
                    '<tpl if="currentUser">',
                    '<div class="cell current" style="width: 6em;">',
                    '<tpl else>',
                    '<div class="cell" style="width: 6em;">',
                    '</tpl>',
                    '<div class="avatar" style="background-image: url({avatar})"></div>',
                    '</div>',
                    '<div class="cell position" style="width: 2em;">',
                    '{position}.',
                    '</div>',
                    '<div class="cell">',
                    '<p class="name"><b>{name}</b></p>',
                    '<span class="pub">{pub}</span>',
                    '</div>',
                    '<div class="cell" style="width: 25%;">',
                    '<b class="points">{points}</b>',
                    '</div>'
                ].join(''),
                plugins: [
                    {
                        xclass: 'Ext.plugin.ListPaging',
                        autoPaging: true,
                        noMoreRecordsText: 'To już koniec'
                    }
                ],
                store: {
                    fields: ['avatar', 'position', 'name', 'pub', 'points', 'month', 'currentUser', 'message'],
                    pageSize: 10,
                    proxy: {
                        type: 'jsonp',
                        reader: {
                            type: 'json',
                            rootProperty: 'rank.data'
                        }
                    },
                    listeners: [
                        {
                            event: 'load',
                            order: 'after',
                            fn: function(store){
                                Ksiazece.app.getController('Ranking').toggleLoadMoreCmp(store);
                            }
                        }
                    ]
                }
            }
        ]
    },

    /**
     * Change store's url
     * @param url
     * @private
     */
    updateUrl: function(url){
        var dataview = this.down('list'),
            store = dataview.getStore(),
            proxy = store.getProxy(),
            message = this.down('message');

        if(url){
            proxy.setUrl(url);
            store.setParams({
                accessToken: localStorage.getItem('token')
            });

            if(Ext.Viewport.getMasked() && Ext.Viewport.getMasked().isHidden()){
                Ext.Viewport.mask({xtype: 'loadmask'});
            }

            store.load(function(records, operation, success){
                if(operation.getResponse()){
                    if(operation.getResponse().authorisation == 0) {
                        Ksiazece.app.getController('Account').logout();
                    } else {
                        if(success) {
                            this.setMessage( operation.getResponse().message );
                        }
                    }
                }

                Ext.Viewport.unmask();
            }, this);
        }
    },

    /**
     * Change message's text
     * @param text
     * @private
     */
    updateMessage: function(msg) {
        var message = this.query('message')[0];

        if(msg) {
            message.setText( msg.text );
            message.setIcon( msg.icon );
        }
    }
});