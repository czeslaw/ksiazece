Ext.define('Ksiazece.view.ranking.Ranking', {
    extend: 'Ext.Panel',

    xtype: 'ranking',

    requires: [
        'Ext.Toolbar',
        'Ext.SegmentedButton',
        'Ksiazece.view.ranking.DataView'
    ],

    config: {
        title: 'Ranking',
        iconCls: 'ranking',
        cls: 'ranking',
        layout: 'vbox',
        items: [
            {
                xtype: 'toolbar',
                ui: 'light',
                cls: 'logo'
            },
            {
                xtype: 'toolbar',
                ui: 'light',
                items: [
                    {
                        xtype: 'segmentedbutton',
                        flex: 1,
                        items: [
                            {
                                pressed: true,
                                text: 'Polska',
                                flex: 1,
                                showCard: 0
                            },
                            {
                                text: 'Kraków',
                                flex: 1,
                                showCard: 1
                            }
                        ]
                    }
                ]
            },
            {
                label: 'rankingcards',
                layout: 'card',
                flex: 1,
                items: [
                    {
                        label: 'global',
                        xtype: 'rankingdataview'
                    }
                ]
            }
        ]
    }
});