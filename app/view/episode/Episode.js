Ext.define('Ksiazece.view.episode.Episode', {
    extend: 'Ext.form.Panel',

    xtype: 'episode',

    requires: [
        'Ext.field.Radio'
    ],

    config: {
        archive: false,

        cls: 'episode',
        flex: 1,
        layout: {
            type: 'vbox'
        },
        items: [
            {
                label: 'episode',
                tpl: '<div class="episode-image" style="height:18em; background-image: url({url});"></div>'
            },
            {
                label: 'episode',
                tpl: '<div class="episode-image" style="height:18em; background-image: url({url});"></div>'
            },
            {
                layout: {
                    type: 'hbox',
                    pack: 'end'
                },
                margin: '0 10 10',
                items: [
                    {
                        xtype: 'button',
                        label: 'fbshare',
                        parentXType: 'episode',
                        ui: 'light',
                        cls: 'fb-share-icn',
                        width: '3.5em'
                    },
                    {
                        xtype: 'button',
                        label: 'emailshare',
                        parentXType: 'episode',
                        ui: 'light',
                        cls: 'mail-share-icn',
                        width: '3.5em'
                    }
                ]
            },
            {
                label: 'question',
                margin: '0 0 0 90'
            },
            {
                xtype: 'fieldset',
                items: []
            },
            {
                xtype: 'button',
                text: 'Dalej',
                margin: '10',
                label: 'sendAnswer',
                ui: 'light',
                docked: Ext.os.is.Android ? false : 'bottom'
            }
        ]
    },

    /**
     * Dodaje pole ukryte episodeID i pola z odpowiedziami
     * @param data Dane z serwera
     */
    setItemsData: function(data){
        var image = this.query('[label=episode]'),
            fieldset = this.query('fieldset')[0],
            question = this.query('[label=question]')[0];

        this.setData(data);

        fieldset.add({
            xtype: 'hiddenfield',
            name: 'episodeID',
            value: data.episodeID
        });

        image[0].setData({url:data.imageUrl});
        image[1].setData({url:data.memUrl});
        question.setHtml(data.question);

        this.addAnswers(data);
    },

    /**
     * Dodaje pola odpowiedzi
     * @param data Dane z serwera
     */
    addAnswers: function(data){
        var fieldset = this.query('fieldset')[0],
            answers = data.answers,
            options = {
                xtype: 'radiofield',
                name : 'answerID',
                labelWidth: '80%',
                labelAlign: 'right'
            };

        for(var i = 0; i < answers.length; i++){
            if(data.userAnswerID){
                if(data.userAnswerID ==  answers[i].answerID){
                    options.checked = true;
                }
            }

            options.value = answers[i].answerID;
            options.label = answers[i].answer;

            fieldset.add(options);
        }
    },

    /**
     * Zwraca dane do wysyłania znajomym
     * @returns {{message: string, url: string}}
     */
    getShareData: function(){
        var data  = {
            message: 'Wiadomość',
            url: 'Brak adresu'
        };

        if(this.getData()){
            data.url  = this.getData().imageUrl;
            data.episodeID = this.getData().episodeID;
        }

        return data;
    }
});