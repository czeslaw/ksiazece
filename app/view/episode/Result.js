Ext.define('Ksiazece.view.episode.Result', {
    extend: 'Ext.form.Panel',

    xtype: 'episoderesult',

    requires: [
        'Ext.field.Radio'
    ],

    config: {
        result: 0,
        response: null,

        cls: 'episode',
        items: [
            {
                xtype: 'panel',
                label: 'result',
                tpl: '{html}',
                cls: 'result-html',
                margin: '0 10'
            },
            {
                xtype: 'button',
                text: 'Zobacz ranking',
                margin: '10',
                label: 'showRanking',
                ui: 'light',
                hidden: true
            },
            {
                label: 'banner',
                margin: '10 0 0',
                tpl: [
                    '<tpl if="link">',
                        '<a href="#" onclick="window.open(\'{link}\', \'_system\');"><img src="{path}"></a>',
                    '<tpl else>',
                        '<img src="{path}">',
                    '</tpl>'
                ].join('')
            }
        ]
    },

    initialize: function() {

        this.on({
            scope: this,
            tap: function(){
                Ksiazece.app.getController('Episodes').reload(this.getResult());
            },
            element:'innerElement',
            delegate:'.result-html'
        });

        this.callParent(arguments);
    },

    updateResponse: function(response){
        this.setResult(response.episode.result);

        if(response.episode.result == '1') {
            this.down('button').show();
        } else {
            this.down('button').hide();
        }

        if(response.episode.html) {
            this.down('[label=result]').setData(response.episode);
        }

        if(response.banner) {
            this.down('[label=banner]').setData(response.banner)
        }
    }
});