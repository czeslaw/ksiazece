Ext.define('Ksiazece.view.episode.List', {
    extend: 'Ext.navigation.View',

    xtype: 'episodes',

    requires: [
        'Ext.Toolbar'
    ],

    config: {
        title: 'Odcinki',
        iconCls: 'episodes',
        cls: 'episodes',
        defaultBackButtonText: '',
        navigationBar: {
            ui: 'light',
            cls: 'logo',
            backButton: {
                iconMask: true,
                iconCls: 'arrow-left',
                ui: 'plain'
            },
            items: [
                {
                    hidden: true,
                    iconMask: true,
                    iconCls: 'settings',
                    label: 'fbshare',
                    align: 'right',
                    parentXType: 'episode',
                    ui: 'plain',
                    hideAnimation: Ext.os.is.Android ? false : {
                        type: 'fadeOut',
                        duration: 200
                    },
                    showAnimation: Ext.os.is.Android ? false : {
                        type: 'fadeIn',
                        duration: 350,
                        delay: 500
                    }
                }
            ]
        },

        items: [
            {
                xtype: 'list',
                flex: 1,
                itemTpl: [
                    '<tpl if="soon">',
                        '<div class="episode-image soon" style="height:6.5em; background-image: url({url});"></div>',
                    '<tpl else>',
                        '<div class="episode-image" style="height:6.5em; background-image: url({url});">{title}</div>',
                    '</tpl>'
                ].join(''),
                store: 'Episodes',
                grouped: true,
                plugins: [
                    {
                        xclass: 'Ext.plugin.ListPaging',
                        autoPaging: true,
                        noMoreRecordsText: 'Nie ma więcej odcinków'
                    }
                ]
            }
        ]
    },

    onBackButtonTap: function(){
        return Ksiazece.app.getController('Episodes').onBack();
    }
});