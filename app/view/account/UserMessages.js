Ext.define('Ksiazece.view.account.UserMessages', {
	extend: 'Ext.Panel',

	xtype: 'usermessages',

	requires: [
		'Ext.Toolbar'
	],

	config: {
		iconCls: 'messages',
		cls: 'messages',
        layout: 'vbox',
		items: [
			{
				xtype: 'toolbar',
				title: 'Powiadomienia',
				ui: 'light',
				cls: 'view-head',
				docked: 'top'
			},
			{
                xtype: 'list',
                flex: 1,
                cls: 'message-list',
                itemTpl: '<div class="item">{text}</div>',
                store: 'Messages',
                itemHeight: '60px',

                plugins: [
                    {
                        xclass: 'Ext.plugin.ListPaging',
                        autoPaging: true,
                        noMoreRecordsText: 'Nie ma więcej wiadomości'
                    }
                ]
			}
		]
	}
});