Ext.define('Ksiazece.view.account.Login', {
    extend: 'Ext.navigation.View',

    xtype: 'login',

    requires: [
        'Ext.form.FieldSet',
        'Ext.field.Email',
        'Ext.field.Password',
        'Ksiazece.extension.FormField'
    ],

    config: {
        defaultBackButtonText: '',
        navigationBar: {
            ui: 'light',
            cls: 'logo',
            backButton: {
                iconMask: true,
                iconCls: 'arrow-left',
                ui: 'plain'
            }
        },
        items: [
            {
                scrollable: null,
                xtype: 'formpanel',
                layout: {
                    type: 'vbox'
                },
                items: [
                    {
                        xtype: 'fieldset',
                        padding: '60 0 0',
                        items: [
                            {
                                xtype: 'formfield',
                                fieldConfig: {
                                    xtype: 'emailfield',
                                    name: 'email',
                                    label: false,
                                    placeHolder: 'E-mail'
                                }
                            },
                            {
                                xtype: 'formfield',
                                fieldConfig: {
                                    xtype: 'passwordfield',
                                    name: 'password',
                                    label: false,
                                    placeHolder: 'Hasło'
                                }
                            }
                        ]
                    },
                    {
                        xtype: 'button',
                        text: 'Zaloguj',
                        margin: '0 10',
                        label: 'login',
                        ui: 'light'
                    },
                    {
                        html: '<p class="forgotten-password">Zapomniałeś hasła?</p>',
                        label: 'showRemindForm'
                    },
                    {
                        margin: '10',
                        docked: Ext.os.is.Android ? false : 'bottom',
                        items: [
                            {
                                xtype: 'button',
                                text: 'Facebook',
                                ui: 'facebook',
                                margin: '0 0 10'
                            },{
                                xtype: 'button',
                                text: 'Załóż konto',
                                label: 'register',
                                ui: 'light'
                            }
                        ]
                    }
                ]
            }
        ]
    },

    initialize: function() {
        this.callParent();

        this.on({
            scope: this,
            tap: function(){
                Ksiazece.app.getController('Account').showRemindForm();
            },
            element:'innerElement',
            delegate:'.forgotten-password'
        });
    }
});