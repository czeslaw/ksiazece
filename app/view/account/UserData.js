Ext.define('Ksiazece.view.account.UserData', {
    extend: 'Ext.Panel',

    xtype: 'userdata',

    requires: [
        'Ext.Toolbar',
        'Ext.SegmentedButton',
        'Ext.field.Select',
        'Ksiazece.extension.AddPhoto',
        'Ksiazece.extension.FormField'
    ],

    config: {
        mode: 'add',

        cls: 'user-data',
        layout: 'vbox',
        items: [
            {
                xtype: 'toolbar',
                ui: 'light',
                items: [
                    {
                        xtype: 'segmentedbutton',
                        flex: 1,
                        items: [
                            {
                                pressed: true,
                                text: 'Dane',
                                flex: 1,
                                showCard: 0
                            },
                            {
                                text: 'Bar',
                                flex: 1,
                                showCard: 1
                            },
                            {
                                text: 'Zdjęcie',
                                flex: 1,
                                showCard: 2,
                                disabled: true
                            }
                        ]
                    }
                ]
            },
            {
                label: 'userdatacards',
                layout: 'card',
                flex: 1,
                items: [
                    {
                        xtype: 'formpanel',
                        label: 'loginData',
                        layout: {
                            type: 'vbox'
                        },
                        items: [
                            {
                                title: 'Dane logowania',
                                xtype: 'fieldset',
                                items: [
                                    {
                                        xtype: 'formfield',
                                        fieldConfig: {
                                            xtype: 'emailfield',
                                            name: 'email',
                                            placeHolder: 'E-mail'
                                        }
                                    },
                                    {
                                        xtype: 'formfield',
                                        fieldConfig: {
                                            xtype: 'emailfield',
                                            name: 'emailConfirm',
                                            placeHolder: 'Powtórz e-mail'
                                        }
                                    },
                                    {
                                        xtype: 'formfield',
                                        fieldConfig: {
                                            xtype: 'passwordfield',
                                            name: 'password',
                                            placeHolder: 'Hasło'
                                        }
                                    },
                                    {
                                        cls: 'text',
                                        html: '<p><i>Wszystkie pola obowiązkowe</i></p>'
                                    }
                                ]
                            },
                            {
                                title: 'Dane osobowe',
                                xtype: 'fieldset',
                                items: [
                                    {
                                        xtype: 'formfield',
                                        fieldConfig: {
                                            xtype: 'textfield',
                                            name: 'name',
                                            placeHolder: 'Imię'
                                        }
                                    },
                                    {
                                        xtype: 'formfield',
                                        fieldConfig: {
                                            xtype: 'textfield',
                                            name: 'surname',
                                            placeHolder: 'Nazwisko'
                                        }
                                    }
                                ]
                            },
                            {
                                xtype: 'button',
                                text: 'Dalej',
                                docked: Ext.os.is.Android ? false : 'bottom',
                                margin: '10',
                                label: 'showNextCard',
                                ui: 'light'
                            }
                        ]
                    },
                    {
                        xtype: 'formpanel',
                        label: 'pubData',
                        layout: {
                            type: 'vbox'
                        },
                        items: [
                            {
                                title: 'Dane Baru',
                                xtype: 'fieldset',
                                items: [
                                    {
                                        xtype: 'formfield',
                                        fieldConfig: {
                                            xtype: 'selectfield',
                                            name: 'userType',
                                            label: false,
                                            usePicker: false,
                                            placeHolder: 'Typ użytkownika',
                                            options: [
                                                {text: 'Jestem - wybierz z listy', value: '0'},
                                                {text: 'Fanem',  value: '1'},
                                                {text: 'Barmanem',  value: '2'},
                                                {text: 'Kelnerem',  value: '3'},
                                                {text: 'Właścicielem',  value: '4'},
                                                {text: 'Managerem',  value: '5'}
                                            ]
                                        }
                                    },
                                    {
                                        xtype: 'formfield',
                                        fieldConfig: {
                                            xtype: 'textfield',
                                            name: 'pubname',
                                            label: false,
                                            placeHolder: 'Nazwa lokalu'
                                        }
                                    },
                                    {
                                        xtype: 'formfield',
                                        fieldConfig: {
                                            xtype: 'textfield',
                                            name: 'city',
                                            label: false,
                                            placeHolder: 'Miasto'
                                        }
                                    },
                                    {
                                        xtype: 'formfield',
                                        fieldConfig: {
                                            xtype: 'textfield',
                                            name: 'street',
                                            label: false,
                                            placeHolder: 'Ulica'
                                        }
                                    },
                                    {
                                        xtype: 'formfield',
                                        fieldConfig: {
                                            xtype: 'textfield',
                                            name: 'streetNo',
                                            label: false,
                                            placeHolder: 'Numer budynku'
                                        }
                                    }
                                ]
                            },
                            {
                                cls: 'text add-city',
                                margin: '0 10 10',
                                html: '<p>Jeżeli miasto, z którego pochodzisz nie znajduje się liście należy skontaktować się z nami drogą <u>e-mailową</u></p>'
                            },
                            {
                                xtype: 'button',
                                text: 'Dalej',
                                docked: Ext.os.is.Android ? false : 'bottom',
                                margin: '10',
                                label: 'showNextCard',
                                ui: 'light'
                            }
                        ]
                    },
                    {
                        title: 'Zdjęcie',
                        items: [
                            {
                                cls: 'text',
                                margin: '10',
                                html: '<h1>Dodaj swoje zdjęcie</h1>'
                            },
                            {
                                xtype: 'addphoto'
                            },
                            {
                                docked: Ext.os.is.Android ? false : 'bottom',
                                layout: 'vbox',
                                items: [
                                    {
                                        xtype: 'button',
                                        text: 'Dalej',
                                        margin: '10',
                                        label: 'showTerms',
                                        ui: 'light'
                                    },
                                    {
                                        xtype: 'button',
                                        text: 'Pomiń',
                                        margin: '0 10 10',
                                        label: 'showTerms',
                                        cls: 'skipAvatar',
                                        hidden: true
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    },

    initialize: function() {
        this.callParent();

        this.on({
            scope: this,
            tap: function(){
                Ksiazece.app.getController('Account').showNewCityForm();
            },
            element:'innerElement',
            delegate:'.add-city'
        });
    },

    setFormValues: function(data){
        var loginForm = this.query('formpanel[label=loginData]')[0],
            pubForm = this.query('formpanel[label=pubData]')[0],
            city = pubForm.query('field[name=city]')[0];

        if(data.city){
            city.setRecord( Ext.create('Ksiazece.model.City', {
                cityID: data.city.cityID,
                name: data.city.name
            }));

            data.city = data.city.name;
        }

        if(data.fbUserId) {
            loginForm.add({
                xtype: 'hiddenfield',
                name: 'fbUserId',
                value: data.fbUserId
            });
        }

        if(data.avatar) {
            loginForm.add({
                xtype: 'hiddenfield',
                name: 'avatar',
                value: data.avatar
            });
        }

        loginForm.setValues(data);
        pubForm.setValues(data);
    },

    getValues: function(){
        var values =  Ext.Object.merge({}, this.query('[label=loginData]')[0].getValues(), this.query('[label=pubData]')[0].getValues());

        if(this.query('[label=pubData]')[0].query('textfield[name=city]')[0].getRecord()) {
            values.city = this.query('[label=pubData]')[0].query('textfield[name=city]')[0].getRecord().get('cityID');
        }

        return values;
    },

    updateMode: function(mode){
        if(mode == 'edit') {
            this.query('[name=email]')['0'].setDisabled('true');
            this.query('[name=emailConfirm]')['0'].setDisabled('true');
            this.query('[name=userType]')['0'].setDisabled('true');

            this.down('[cls=text]').hide();

            this.query('[label=showTerms]')[0].setText('Zapisz');

            this.enablePhotoTab();
        }

        if(mode == 'add'){
            this.down('button[cls=skipAvatar]').show();
        } else {
            this.down('button[cls=skipAvatar]').hide();
        }

        if(mode == 'facebook') {
            this.query('[name=password]')['0'].hide();
        }
    },

    enablePhotoTab: function(){
        this.query('segmentedbutton')[0].getItems().getAt(2).setDisabled(false);
    }
});