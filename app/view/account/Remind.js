Ext.define('Ksiazece.view.account.Remind', {
    extend: 'Ext.form.Panel',

    xtype: 'remind',

    requires: [
        'Ext.Img',
        'Ext.form.FieldSet',
        'Ext.field.Email',
        'Ext.field.Password'
    ],

    config: {
        layout: {
            type: 'vbox'
        },

        items: [
            {
                xtype: 'fieldset',
                padding: '80 0 0',
                items: [
                    {
                        xtype: 'formfield',
                        fieldConfig: {
                            xtype: 'emailfield',
                            name: 'email',
                            label: false,
                            placeHolder: 'Adres email'
                        }
                    }
                ]
            },
            {
                xtype: 'button',
                ui: 'light',
                text: 'Przypomnij hasło',
                margin: '0 10'
            }
        ]
    }
});