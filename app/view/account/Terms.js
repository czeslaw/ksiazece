Ext.define('Ksiazece.view.account.Terms', {
    extend: 'Ext.Panel',

    xtype: 'terms',

    config: {
        cls: 'terms',
        layout: 'vbox',
        items: [
            {
                flex: 1,
                scrollable: true,
                cls: 'text',
                margin: '10 10 0',
                html: [
                    '<h1>Regulamin</h1>',
                    '<ol>',
                        '<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ante diam, mollis eu rutrum sit amet, interdum nec tellus. Integer ligula nibh, cursus congue diam id, molestie semper urna. Nam molestie ipsum id imperdiet volutpat. Ut venenatis, justo condimentum mattis cursus, purus ipsum molestie libero, vitae bibendum sapien nisl vel dolor. Pellentesque suscipit velit tortor, quis lobortis sem pulvinar eget. Phasellus molestie tortor at fermentum blandit. Vivamus mattis urna nec vestibulum mattis. Nulla id elementum ipsum.</li>',
                        '<li>Proin venenatis tristique tristique. Nunc consectetur sapien sit amet metus molestie malesuada. Fusce vitae arcu ac erat congue fermentum. Phasellus pellentesque massa diam, varius pulvinar diam ultricies eget. Nullam id enim in quam auctor porta. Sed et lectus scelerisque, accumsan turpis eu, ornare velit. Integer gravida dui orci, eget posuere nunc ultrices ac.</li>',
                        '<li>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Phasellus sollicitudin mauris id est laoreet tristique sed vel nisi. Proin interdum sagittis felis, vitae feugiat eros dictum a. Donec pellentesque ullamcorper urna, nec tristique magna rutrum ac. Nunc fermentum lectus ut ligula vulputate interdum. Sed eget urna ullamcorper neque auctor lobortis. Donec et augue sit amet mauris molestie ultrices eget ac justo.</li>',
                    '</ol>'
                ].join('')
            },
            {
                docked: Ext.os.is.Android ? false : 'bottom',
                layout: 'vbox',
                items: [
                    {
                        xtype: 'button',
                        text: 'Akceptuję',
                        margin: '10',
                        label: 'acceptTerms',
                        ui: 'light'
                    },
                    {
                        xtype: 'button',
                        text: 'Odrzucam',
                        margin: '0 10 10',
                        label: 'declineTerms',
                        ui: 'reversed'
                    }
                ]
            }
        ]
    }
});