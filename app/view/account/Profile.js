Ext.define('Ksiazece.view.account.Profile', {
    extend: 'Ext.navigation.View',

    xtype: 'profile',

    config: {
        loaded: false,

        title: 'Profil',
        iconCls: 'profile',
        defaultBackButtonText: '',
        navigationBar: {
            ui: 'light',
            cls: 'logo',
            backButton: {
                iconMask: true,
                iconCls: 'arrow-left',
                ui: 'plain'
            },
            items: [
                {
                    xtype: 'button',
                    text: 'Wyloguj',
                    align: 'left',
                    showView: 'Login',
                    hideAnimation: Ext.os.is.Android ? false : {
                        type: 'fadeOut',
                        duration: 200
                    },
                    showAnimation: Ext.os.is.Android ? false : {
                        type: 'fadeIn',
                        duration: 350,
                        delay: 500
                    }
                },
                {
                    xtype: 'button',
                    text: 'Edytuj',
                    align: 'right',
                    showView: 'UserData',
                    hideAnimation: Ext.os.is.Android ? false : {
                        type: 'fadeOut',
                        duration: 200
                    },
                    showAnimation: Ext.os.is.Android ? false : {
                        type: 'fadeIn',
                        duration: 350,
                        delay: 500
                    }
                }
            ]
        },
        items: [
            {
                xtype: 'user'
            }
        ],
        listeners: [
            {
                event: 'activeitemchange',
                order: 'before',
                fn: function( panel,  value, oldValue, eOpts ) {
                    if(value && value.isXType('user')){

                        if(value.getData().successMessage) {
                            value.showEditSuccessMessage();
                            value.setData({user: {avatar: oldValue.query('addphoto')[0].getAvatarUrl()}})
                        }

                    }
                }
            }
        ]
    }
});