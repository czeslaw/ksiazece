Ext.define('Ksiazece.view.account.User', {
    extend: 'Ext.Panel',

    xtype: 'user',

    requires: [
        'Ksiazece.extension.Message'
    ],

    config: {
        userData: {},
        scrollable: true,
        layout: 'vbox',
        items: [
            {
                xtype: 'toolbar',
                title: 'Profil',
                ui: 'light',
                items: [
                    {
                        xtype: 'spacer'
                    },
                    {
                        iconMask: true,
                        iconCls: 'envelope',
                        ui: 'plain',
                        showView: 'UserMessages',
                        label: 'inbox',
                        hideAnimation: Ext.os.is.Android ? false : {
                            type: 'fadeOut',
                            duration: 200
                        },
                        showAnimation: Ext.os.is.Android ? false : {
                            type: 'fadeIn',
                            duration: 350,
                            delay: 500
                        }
                    }
                ]
            },
            {
                xtype: 'message',
                label: 'edit',
                hidden: true
            },
            {
                label: 'userData',
                cls: 'user',
                margin: '10 0',
                tpl: [
                    '<div class="view-subhead">Informacje</div>',
                    '<div class="avatar" style="background-image: url({avatar})"></div>',
                    '<h1>{name} {surname}</h1>',
                    '<span class="pub">{pubname}</span>',
                    '<span class="points">{userPoints} pkt</span>'
                ].join('')
            },
            {
                xtype: 'message',
                label: 'points',
                hidden: true
            },
            {
                label: 'tips',
                cls: 'tips',
                tpl: [
                    '<h2>Zdobądź więcej punktów</h2><ul>',
                    '<tpl for="tips">',
                    '<tpl if="title">',
                        '<tpl if="done">',
                            '<li class="done" data-view="{showView}">',
                        '<tpl else>',
                            '<li data-view="{showView}">',
                        '</tpl>',
                            '<h3>{title}</h3>',
                        '<tpl if="missing">',
                            '<p>{maxPoints} pkt / Brakuje Ci {missing} pkt</p>',
                        '<tpl else>',
                            '<p>{maxPoints} pkt</p>',
                        '</tpl>',
                        '</li>',
                    '</tpl>',
                    '</tpl></ul>'
                ].join('')
            }
        ]
    },

    initialize: function() {

        this.on({
            scope: this,
            tap: function(event){
                Ksiazece.app.getController('Profile').onTipsTap(event);
            },
            element:'innerElement',
            delegate:'.tips li'
        });

        this.callParent(arguments);
    },

    showEditSuccessMessage: function(){
        var message = this.query('message[label=edit]')[0],
            data = this.getData();

        if(data.successMessage){
            message.setText(data.successMessage);
            message.show();

            setTimeout(function(){
                message.hide();
            }, 3000);
        }
    },

    updateData: function(data){
        var user = this.query('[label=userData]')[0],
            msg = this.query('message[label=points]')[0],
            tips = this.query('[label=tips]')[0];

        if(this.getUserData()) {
            data = Ext.merge(this.getUserData(), data);
        }

        if(data.user){
            user.setData(data.user);

            if(data.user.city) {
                Ksiazece.app.getController('Ranking').updateCity(data.user.city);
            }
        }
        if(data.message) {
            msg.setText(data.message.text);
            msg.setIcon(data.message.icon);
            msg.show();
        } else {
            msg.hide();
        }

        if(data.tips) {
            tips.setData(data);
        }

        this.setUserData(data);
    }
});