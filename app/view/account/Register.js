Ext.define('Ksiazece.view.account.Register', {
    extend: 'Ext.Panel',

    xtype: 'register',

    requires: [
        'Ext.field.Number',
        'Ksiazece.extension.Message'
    ],

    config: {
        cls: 'register',
        layout: {
            type: 'vbox'
        },
        items: [
            {
                xtype: 'message',
                icon: 'caution',
                text: [
                    '<p>Aby korzystać z aplikacji musisz mieć ukończone 18 lat.</p>',
                    '<p>Wprowadź swój wiek poniżej.</p>'
                ].join('')
            },
            {
                xtype: 'formpanel',
                cls: 'age-verification',
                scrollable: false,
                height: '14em',
                items: [
                    {
                        xtype: 'fieldset',
                        layout: 'hbox',
                        defaults: {
                            clearIcon: false,
                            xtype: (Ext.os.is.Android  && Ext.os.version.lt('4.2'))? 'textfield' : 'numberfield',
                            labelAlign: 'top',
                            flex: 1
                        },
                        items: [
                            {
                                name: 'day',
                                label: 'Dzień',
                                minValue: 1,
                                maxValue: 31,
                                placeHolder: 'DD',
                                flex: 2,
                                maxLength: 2
                            },
                            {
                                name: 'month',
                                label: 'Miesiąc',
                                minValue: 1,
                                maxValue: 12,
                                placeHolder: 'MM',
                                flex: 2,
                                maxLength: 2
                            },
                            {
                                name: 'year',
                                label: 'Rok',
                                placeHolder: 'RRRR',
                                flex: 3,
                                maxLength: 4
                            }
                        ]
                    },{
                        label: 'message',
                        flex: 1,
                        margin: '0 10',
                        cls: 'text-big',
                        tpl: '<p class="text">{msg}</p>'
                    }
                ]
            },
            {
                docked: Ext.os.is.Android ? false : 'bottom',
                layout: 'vbox',
                margin: '10',
                items: [
                    {
                        cls: 'text',
                        xtype: 'panel',
                        margin: '0 0 10',
                        html: '<p>Dowiedz się więcej o alkoholu na <a href="#" onclick="window.open(\'http://www.abcalkoholu.pl\', \'_system\');">www.abcalkoholu.pl</a></p><p>Jeżeli uważasz, że reklama naszego produktu narusza zasady Kodeksu Etyki Reklamy, <a href="#" onclick="window.open(\'http://radareklamy.org/zloz-skarge/skarga-konusmencka.html\', \'_system\');">kliknij</a></p>'
                    },
                    {
                        xtype: 'button',
                        text: 'Dalej',
                        label: 'verify-age',
                        ui: 'light'
                    }
                ]
            }
        ]
    }
});