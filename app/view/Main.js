Ext.define('Ksiazece.view.Main', {
    extend: 'Ext.Panel',
    xtype: 'main',

    config: {
        layout: 'card',
        items: [
            {
                xtype: 'login'
            }
        ],
        listeners: [
            {
                event: 'activeitemchange',
                order: 'after',
                fn: function( panel,  value, oldValue, eOpts ) {
                    if(oldValue.isXType('tabs')){
                        panel.remove(oldValue);
                    }
                }
            }
        ]
    }
});
