Ext.define('Ksiazece.view.Tabs', {
    extend: 'Ext.TabPanel',

    xtype: 'tabs',

    config : {
        tabBarPosition: 'bottom',
        tabBar: {
            defaults: {
                flex: 1
            }
        },
        items: [
            {
                xtype: 'episodes'
            },
            {
                xtype: 'specials'
            },
            {
                xtype: 'ranking'
            },
            {
                xtype: 'generator'
            },
            {
                xtype: 'profile'
            }
        ]
    },

    setBadge: function(text, viewIndex){
        this.query('tabbar')[0].getAt(viewIndex).setBadgeText(text);
    }
});