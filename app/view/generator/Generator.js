Ext.define('Ksiazece.view.generator.Generator', {
    extend: 'Ext.navigation.View',

    xtype: 'generator',

    requires: [
        'Ext.Toolbar',
        'Ksiazece.extension.BigButton'
    ],

    config: {
        title: 'Generator',
        iconCls: 'generator',
        cls: 'generator',
        defaultBackButtonText: '',
        navigationBar: {
            ui: 'light',
            cls: 'logo',
            backButton: {
                iconMask: true,
                iconCls: 'arrow-left',
                ui: 'plain'
            },
            items: [

            ]
        },
        items: [
            {
                layout: 'vbox',
                scrollable: true,
                flex: 1,
                items: [
                    {
                        xtype: 'toolbar',
                        title: 'Generator Memów',
                        ui: 'light',
                        cls: 'view-head',
                        docked: 'top'
                    },
                    {
                        xtype: 'button',
                        cls: 'add-meme',
                        iconCls: 'photo-camera',
                        iconMask: true,
                        label: 'choose',
                        variableHeights: true
                    },
                    {
                        ui: 'light',
                        cls: 'view-subhead',
                        html: 'Stwórz nowego mema'
                    },
                    {
                        xtype: 'button',
                        cls: 'show-list',
                        iconCls: 'image',
                        iconMask: true,
                        label: 'showList',
                        variableHeights: true
                    },
                    {
                        ui: 'light',
                        cls: 'view-subhead',
                        html: 'Lista Twoich memów'
                    }


                ]
            }
        ]
    },

    onBackButtonTap: function(){
        return Ksiazece.app.getController('Generator').onBack();
    }
});