Ext.define('Ksiazece.view.generator.Preview', {
    extend: 'Ext.form.Panel',

    xtype: 'generatorpreview',

    config: {
        cls: 'preview',
        flex: 1,
        layout: {
            type: 'vbox'
        },
        items: [
            {
                xtype: 'toolbar',
                title: 'Twój nowy mem',
                ui: 'light',
                cls: 'view-head',
                docked: 'top',
                items: [
                    {
                        xtype: 'spacer'
                    },
                    {
                        iconMask: true,
                        iconCls: 'settings',
                        label: 'fbshare',
                        parentXType: 'generatorpreview',
                        ui: 'plain'
                    }
                ]
            },
            {
                xtype: 'message',
                text: 'Mem został zapisany',
                hidden: true
            },
            {
                html: '<canvas id="canvas"></canvas>',
                hidden: true
            },
            {
                xtype: 'image',
                src: 'resources/images/mem-preview.jpg',
                flex: 1
            },
            {
                docked: Ext.os.is.Android ? false : 'bottom',
                layout: 'vbox',
                items: [
                    {
                        layout: 'hbox',
                        margin: '10 10 0',
                        items: [
                            {
                                xtype: 'button',
                                text: 'Edytuj',
                                label: 'change',
                                ui: 'light',
                                flex: 1
                            },
                            {
                                xtype: 'button',
                                text: 'Pobierz',
                                label: 'save',
                                parentXType: 'generatorpreview',
                                ui: 'light',
                                flex: 1
                            }
                        ]
                    },
                    {
                        layout: 'hbox',
                        margin: '0 10 10',
                        items: [
                            {
                                xtype: 'button',
                                label: 'fbshare',
                                parentXType: 'generatorpreview',
                                ui: 'light',
                                cls: 'fb-share-icn',
                                flex: 1
                            },
                            {
                                xtype: 'button',
                                label: 'emailshare',
                                parentXType: 'generatorpreview',
                                ui: 'light',
                                cls: 'mail-share-icn',
                                flex: 1
                            }
                        ]
                    }
                ]
            }
        ]
    },

    setPreview: function(url){
        var image  = this.query('image')[0];

        image.setSrc(url);
    },

    /**
     * Zwraca dane do wysyłania znajomym
     * @returns {{message: string, url: string}}
     */
    getShareData: function(){
        if(Ksiazece.app.getController('Generator').getMemRecord()){
            return  {
                message: 'Wiadomość',
                url: Ksiazece.app.getController('Generator').getMemRecord().get('memPicture'),
                memID: Ksiazece.app.getController('Generator').getMemRecord().get('memID')
            }
        } else {
            return  {
                message: 'Wiadomość',
                url: 'Brak adresu'
            }
        }
    }
});