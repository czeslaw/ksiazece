Ext.define('Ksiazece.view.generator.Edit', {
    extend: 'Ext.form.Panel',

    xtype: 'generatoredit',

    config: {
        preview: null,
        memOnText: '',

        cls: 'edit',
        flex: 1,
        layout: {
            type: 'vbox'
        },
        items: [
            {
                xtype: 'toolbar',
                title: 'Generator Memów',
                ui: 'light',
                cls: 'view-head',
                docked: 'top'
            },
            {
                xtype: 'image',
                src: 'resources/images/mem.jpg',
                flex: 1
            },
            {
                label: 'counter',
                margin: '5 10',
                tpl: ' Pozostało znaków: {charsLeft}'
            },
            {
                xtype: 'fieldset',
                items: [
                    {
                        xtype: 'textareafield',
                        label: false,
                        placeHolder: 'Tekst',
                        maxRows: 4,
                        name: 'text1',
                        maxLength: 50
                    }
                ]
            },
            {
                docked: Ext.os.is.Android ? false : 'bottom',
                xtype: 'button',
                text: 'Dalej',
                margin: '0 10 10',
                label: 'send',
                ui: 'light'
            }
        ]
    },

    updateMemOnText: function(text){
        if(text){
            this.query('textareafield')[0].setValue(text);
        }
    },

    updatePreview: function( src ){
        var image = this.query('image')[0],
            counter = this.query('[label=counter]')[0];

        counter.setData({charsLeft: 50});

        if(src){
            image.setSrc( src.path );
            image.setRecord( src );
        }
    }
});