Ext.define('Ksiazece.view.generator.List', {
    extend: 'Ext.List',

    xtype: 'generatorlist',

    requires: [
        'Ext.Toolbar'
    ],

    config: {
        flex: 1,
        xtype: 'list',
        items: [
            {
                xtype: 'toolbar',
                title: 'Lista memów',
                ui: 'light',
                cls: 'view-head',
                docked: 'top'
            }
            ],
        loadingText: 'Ładuję...',
        emptyText: "Nie ma jeszcze memów",
        itemTpl: [
            '<div class="meme-item-box"><img src="{memTopPicture}" class="meme-item-listning"><span>{text}</span><span class="btn btn-more">Więcej</span></div>'
        ].join(''),
        store: {
            autoLoad: false,
            fields: ['memID', 'memPicture', 'memTopPicture', 'pictureID', 'picture', 'text', 'link'],
            proxy: {
                type: 'jsonp',
                url: Ksiazece.Config.getHost() + Ksiazece.Config.getMemListUrl(),
                reader: {
                    type: 'json',
                    rootProperty: 'memList'
                }
            }
        }
    }
});