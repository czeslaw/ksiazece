Ext.define('Ksiazece.view.generator.Choose', {
    extend: 'Ext.Panel',

    xtype: 'generatorchoose',

    config: {
        preview: null,
        mode: 'new',

        cls: 'choose',
        flex: 1,
        layout: {
            type: 'vbox'
        },
        items: [
            {
                xtype: 'toolbar',
                title: 'Generator Memów',
                ui: 'light',
                cls: 'view-head',
                docked: 'top'
            },
            {
                flex: 2,
                xtype: 'image',
                src: 'resources/images/mem.jpg'
            },
            {
                height: '7em',
                xtype: 'dataview',
                store: {
                    autoLoad: false,
                    fields: ['id', 'path'],
                    proxy: {
                        url: Ksiazece.Config.getHost() + Ksiazece.Config.getMemImagesUrl(),
                        type: 'jsonp',
                        reader: {
                            type: 'json',
                            rootProperty: 'mem.images'
                        }
                    }
                },
                inline: {
                    wrap: false
                },
                scrollable: 'horizontal',
                itemTpl: '<div><img src="{path}"></div>'
            },
            {
                docked: Ext.os.is.Android ? false : 'bottom',
                xtype: 'button',
                text: 'Wybieram',
                margin: '0 10 10',
                label: 'send',
                ui: 'light'
            }
        ]
    },

    initialize: function(){
        var store = this.query('dataview')[0].getStore();

        store.setParams({
            accessToken: localStorage.getItem('token')
        });

        if(Ext.Viewport.getMasked() && Ext.Viewport.getMasked().isHidden()){
            Ext.Viewport.mask({xtype: 'loadmask'});
        }

        store.load(function(records, operation, success){
            if(operation.getResponse().authorisation == 0) {
                    Ksiazece.app.getController('Account').logout();
            } else {
                if(this.getMode() == 'new'){
                    this.setPreview( operation.getResponse().mem.images[0] );
                }
            }

            Ext.Viewport.unmask();
        }, this);

        this.callParent(arguments);
    },

    updatePreview: function( src ){
        var image = this.query('image')[0];

        if(src){
            image.setSrc( src.path );
            image.setRecord( src );
        }
    }
});