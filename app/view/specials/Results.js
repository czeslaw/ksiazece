Ext.define('Ksiazece.view.specials.Results', {
    extend: 'Ext.Panel',

    xtype: 'specialsresutls',

    config: {
        cls: 'ranking',
        flex: 1,
        layout: {
            type: 'vbox'
        },
        items: [
            {
                xtype: 'message',
                text: 'W tym miesiącu gramy o bilet na koncert <b>Coldplay</b>'
            },
            {
                flex: 1,
                xtype: 'list',
                loadingText: 'Ładuję...',
                disableSelection: true,
                itemTpl: [
                    '<tpl if="currentUser">',
                    '<div class="cell current" style="width: 20%;">',
                    '<tpl else>',
                    '<div class="cell" style="width: 20%;">',
                    '</tpl>',
                    '<div class="avatar" style="background-image: url({avatar})"></div>',
                    '</div>',
                    '<div class="cell position" style="width: 10%;">',
                    '{position}.',
                    '</div>',
                    '<div class="cell" style="width: 55%;">',
                    '<p><b>{name}</b></p>',
                    '{pub}',
                    '</div>',
                    '<div class="cell" style="width: 25%;">',
                    '<b>{points}</b>',
                    '</div>'
                ].join('')
            }
        ]
    },

    setItemsData: function(data){
        var message = this.query('message')[0],
            list = this.query('list')[0];

        console.log( data );

        list.setData(data.data);

        message.setText(data.message.text);
        message.setIcon(data.message.icon);
    }
});