Ext.define('Ksiazece.view.specials.Photo', {
    extend: 'Ext.form.Panel',

    xtype: 'specialsphoto',

    config: {
        cls: 'joke',
        flex: 1,
        layout: {
            type: 'vbox'
        },
        items: [
            {
                xtype: 'message'
            },
            {
                label: 'question',
                margin: '0 10 10'
            },
            {
                xtype: 'addphoto'
            },
            {
                docked: Ext.os.is.Android ? false : 'bottom',
                xtype: 'button',
                text: 'Wyślij',
                margin: '10',
                label: 'send',
                ui: 'light'
            }
        ]
    },

    setItemsData: function(data){
        var message = this.query('message')[0],
            question = this.query('[label=question]')[0];

        message.setText(data.message.text);
        message.setIcon(data.message.icon);

        question.setHtml(data.question);

        this.setData(data);
    }
});