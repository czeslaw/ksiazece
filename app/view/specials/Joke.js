Ext.define('Ksiazece.view.specials.Joke', {
    extend: 'Ext.form.Panel',

    xtype: 'specialsjoke',

    requires: [
        'Ext.field.TextArea'
    ],

    config: {
        cls: 'joke',
        flex: 1,
        layout: {
            type: 'vbox'
        },
        items: [
            {
                xtype: 'message'
            },
            {
                label: 'desc',
                margin: '10'
            },
            {
                label: 'question',
                margin: '0 10'
            },
            {
                label: 'counter',
                margin: '5 10',
                tpl: ' Pozostało znaków: {charsLeft}'
            },
            {
                xtype: 'fieldset',
                items: [
                    {
                        xtype: 'textareafield',
                        label: false,
                        maxRows: 4,
                        name: 'answer',
                        maxLength: 360
                    }
                ]
            },
            {
                xtype: 'button',
                text: 'Wyślij',
                margin: '0 10',
                label: 'send',
                ui: 'light'
            }
        ]
    },

    setItemsData: function(data){
        var message = this.query('message')[0],
            counter = this.query('[label=counter]')[0],
            fieldset = this.query('fieldset')[0],
            desc = this.query('[label=desc]')[0],
            question = this.query('[label=question]')[0];

        fieldset.add({
            xtype: 'hiddenfield',
            name: 'specialID',
            value: data.data.specialID
        });

        counter.setData({charsLeft: 360});

        message.setText(data.message.text);
        message.setIcon(data.message.icon);

        desc.setHtml(data.description);
        question.setHtml(data.data.question);
    }
});