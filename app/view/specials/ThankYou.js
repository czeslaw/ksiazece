Ext.define('Ksiazece.view.specials.ThankYou', {
    extend: 'Ext.Panel',

    xtype: 'specialsthankyou',

    config: {
        cls: 'thank-you',
        flex: 1,
        layout: {
            type: 'vbox'
        },
        items: [
            {
                xtype: 'message'
            },
            {
                label: 'thankYouText',
                cls: 'thank-you',
                centered: true,
                width: '100%',
                margin: 10,
                html: [
                    '<h1>Dziękujemy!</h1>',
                    '<p>Nagranie zostało przesłane</p>'
                ].join('')
            },
            {
                xtype: 'button',
                docked: Ext.os.is.Android ? false : 'bottom',
                hidden: true,
                text: 'Podziel się',
                margin: '10',
                ui: 'light'
            }
        ]
    },

    setItemsData: function(data){
        var message = this.down('message'),
            button = this.down('button'),
            text = this.down('[label=thankYouText]');

        if(data.type) {
            if(data.type == 'video' || data.type == 'photo' || data.type == 'sound' ){
                button.show();
            } else {
                button.hide();
            }

            console.log( data );

            text.addCls(data.type);
        }

        if(data.specialMessage) {
            message.setText(data.specialMessage);
        }
    }
});