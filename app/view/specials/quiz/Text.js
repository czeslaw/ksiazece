Ext.define('Ksiazece.view.specials.quiz.Text', {
    extend: 'Ext.form.Panel',

    xtype: 'specialsquiztext',

    requires: [
        'Ext.field.TextArea'
    ],

    config: {
        step: 0,

        cls: 'quiz-text',
        flex: 1,
        layout: {
            type: 'vbox'
        },
        items: [
            {
                xtype: 'message'
            },
            {
                label: 'question',
                margin: '0 10'
            },
            {
                xtype: 'fieldset',
                items: [
                    {
                        xtype: 'textareafield',
                        label: false,
                        maxRows: 4,
                        name: 'answer'
                    }
                ]
            },
            {
                xtype: 'button',
                text: 'Dalej',
                margin: '10',
                label: 'nextStep',
                ui: 'light',
                docked: Ext.os.is.Android ? false : 'bottom'
            }
        ]
    },

    setItemsData: function(data){
        var message = this.query('message')[0],
            fieldset = this.query('fieldset')[0],
            question = this.query('[label=question]')[0],
            stepData = data.data.steps[this.getStep()];

        fieldset.add({
            xtype: 'hiddenfield',
            name: 'specialID',
            value: data.data.specialID
        });

        fieldset.add({
            xtype: 'hiddenfield',
            name: 'questionID',
            value: stepData.questionID
        });

        message.setText(data.message.text);
        message.setIcon(data.message.icon);

        question.setHtml(stepData.question);

        this.setData(data);
    },

    isValid: function(){
        return this.getValues().answer != '';
    }
});