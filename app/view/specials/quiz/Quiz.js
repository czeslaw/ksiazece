Ext.define('Ksiazece.view.specials.quiz.Quiz', {
    extend: 'Ext.form.Panel',

    xtype: 'specialsquiz',

    requires: [
        'Ext.field.Radio'
    ],

    config: {
        step: 0,

        cls: 'joke',
        flex: 1,
        layout: {
            type: 'vbox'
        },
        items: [
            {
                xtype: 'message'
            },
            {
                label: 'question',
                margin: '0 10'
            },
            {
                xtype: 'fieldset',
                items: [
                ]
            },
            {
                xtype: 'button',
                text: 'Dalej',
                margin: '10',
                label: 'nextStep',
                ui: 'light',
                docked: Ext.os.is.Android ? false : 'bottom'
            }
        ]
    },

    setItemsData: function(data){
        var message = this.query('message')[0],
            fieldset = this.query('fieldset')[0],
            question = this.query('[label=question]')[0],
            stepData = data.data.steps[this.getStep()];

        fieldset.add({
            xtype: 'hiddenfield',
            name: 'specialID',
            value: data.data.specialID
        });

        fieldset.add({
            xtype: 'hiddenfield',
            name: 'questionID',
            value: stepData.questionID
        });

        message.setText(data.message.text);
        message.setIcon(data.message.icon);

        question.setHtml(stepData.question);

        this.addAnswers(stepData.answers);
    },

    addAnswers: function(answers){
        var fieldset = this.query('fieldset')[0];

        for(var i = 0; i < answers.length; i++){
            fieldset.add({
                xtype: 'radiofield',
                name : 'answerID',
                value: answers[i].answerID,
                label: answers[i].answer,
                labelWidth: '80%',
                labelAlign: 'right'
            });
        }
    },

    isValid: function(){
        return typeof this.getValues().answerID != 'undefined';
    }
});