Ext.define('Ksiazece.view.specials.Message', {
    extend: 'Ext.Panel',

    xtype: 'specialsmessage',

    config: {
        flex: 1,
        layout: {
            type: 'vbox',
            align: 'stretch',
            pack: 'center'
        },
        items: [
            {
                xtype: 'message'
            }
        ]
    },

    setItemsData: function(data){
        var message = this.query('message')['0'];

        message.setText(data.message);
    }
});