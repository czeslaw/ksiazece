Ext.define('Ksiazece.view.specials.Specials', {
    extend: 'Ext.navigation.View',

    xtype: 'specials',

    requires: [
        'Ext.field.Hidden'
    ],

    config: {
        title: 'Zad. specjalne',
        iconCls: 'specials',
        cls: 'specials',
        defaultBackButtonText: '',
        navigationBar: {
            ui: 'light',
            cls: 'logo',
            backButton: {
                iconMask: true,
                iconCls: 'arrow-left',
                ui: 'plain'
            }
        }
    }
});