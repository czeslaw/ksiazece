Ext.define('Ksiazece.store.Cities', {
    extend: 'Ext.data.Store',

    requires: [
        'Ext.data.proxy.JsonP'
    ],

    config: {
        fields: ['cityID', 'name'],

        autoLoad: false,

        proxy: {
            type: 'jsonp',
            reader: {
                rootProperty: "cities"
            },
            url: Ksiazece.Config.getHost() + Ksiazece.Config.getCitiesListUrl()
        }
    }
});