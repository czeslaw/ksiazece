Ext.define('Ksiazece.store.Messages', {
	extend: 'Ext.data.Store',

	requires: [
		'Ext.data.proxy.JsonP'
	],

	config: {
		model: 'Ksiazece.model.Message',
		autoLoad: false,
		pageSize: 14,

		proxy: {
			type: 'jsonp',
			reader: {
				rootProperty: "messages"
			},
			url: Ksiazece.Config.getHost() + Ksiazece.Config.getMessageListUrl()
		}
	}
});