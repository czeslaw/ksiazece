Ext.define('Ksiazece.store.Episodes', {
    extend: 'Ext.data.Store',

    requires: [
        'Ext.data.proxy.JsonP'
    ],

    config: {
        model: 'Ksiazece.model.Episode',
        autoLoad: false,
        pageSize: 4,

        grouper: {
            property: 'header',
            sortProperty: 'number',
            direction: 'desc'
        },

        proxy: {
            type: 'jsonp',
            reader: {
                rootProperty: "episodes"
            },
            url: Ksiazece.Config.getHost() + Ksiazece.Config.getEpisodesListURL()
        },

        listeners: [
            {
                event: 'load',
                order: 'after',
                fn: function(store){
                    Ksiazece.app.getController('Episodes').toggleLoadMoreCmp(store);
                }
            }
        ]
    }
});