Ext.define('Ksiazece.extension.FormField', {
    extend: 'Ext.Container',

    xtype: 'formfield',

    config: {
        /**
         * Ustawienia dla pola formularza
         */
        fieldConfig: {},

        valid: null,

        cls: 'formfield',
        layout: 'vbox'
    },

    initialize: function() {
        var fieldConfig = this.getFieldConfig();

        this.add([ fieldConfig, {
            cls: 'msg',
            label: 'errorMsg',
            tpl: '{errorMsg}',
            margin: '0 2 10'
        }]);

        this.callParent();
    },

    /**
     * Ustawia status (poprawny, niepoprawny) i wiadomość w zależności od danych zwróconych z serwera
     * @param data Dane z serwera
     */
    setStatus: function(data){
        if(data.validated == 0) {
            this.setValid(false);
            this.setErrorMsg(data.validatedMessages);
        } else {
            this.setValid(true);
            this.setErrorMsg('');
        }
    },

    setErrorMsg: function(msg){
        this.query('[label=errorMsg]')[0].setData({'errorMsg': msg});
    },

    updateValid: function(valid){
        var field = this.query('field')[0];

        if(valid === true) {
            field.removeCls('invalid');
            field.addCls('valid');
        } else if(valid === false) {
            field.removeCls('valid');
            field.addCls('invalid');
        } else {
            field.removeCls('valid');
            field.removeCls('invalid');
        }
    }
});