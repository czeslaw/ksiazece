Ext.define('Ksiazece.extension.BigButton', {
    extend: 'Ext.Component',

    xtype: 'bigbutton',

    config: {
        cls: 'big-button',

        text: null,

        icon: null
    },

    template: [
        {
            tag: 'div',
            cls: 'icon',
            reference: 'iconElement'
        },
        {
            tag: 'p',
            cls: 'text',
            reference: 'textElement'
        }
    ],

    initialize: function() {
        this.callParent();

        this.element.on({
            scope: this,
            tap: 'onTap'
        });
    },

    onTap: function(e){
        this.fireEvent('tap', [this, e]);
    },


    /**
     * @private
     */
    updateIcon: function(icon, oldIcon) {
        var element = this.iconElement;

        if (icon) {
            element.addCls(icon);
        }

        if(oldIcon) {
            element.removeCls(oldIcon);
        }
    },

    /**
     * @private
     */
    updateText: function(text) {
        var element = this.textElement;

        if (element) {
            element.setHtml(text);
        }
    }
});