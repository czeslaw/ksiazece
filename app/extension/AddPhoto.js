Ext.define('Ksiazece.extension.AddPhoto', {
    extend: 'Ext.Component',

    xtype: 'addphoto',

    config: {
        avatarUrl: '',

        cls: 'add-photo'
    },

    template: [{
        tag: 'div',
        cls: 'border',
        children:[
            {
                tag: 'div',
                cls: 'photo-elem',
                reference: 'photoElement'
            }
        ]
    }],

    initialize: function() {
        this.element.on({
            tap: 'askForPhoto',
            scope: this
        });

        this.callParent(arguments);
    },

    askForPhoto: function(){
        var that = this;

        navigator.camera.getPicture(function(imageURI){
            that.onCaptureSuccess(imageURI, that);
        }, this.onFail, {
            destinationType: Camera.DestinationType.FILE_URI,
            sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
            correctOrientation: true
        });
    },

    /**
     *
     * @param data
     */
    setPhotoAsBackground: function(data){
        var jsonData;

        if(typeof data.response === 'string') {
            jsonData = Ext.JSON.decode(data.response);
        } else {
            jsonData = data;
        }

        this.setAvatarUrl(jsonData.avatar);

        this.photoElement.setHtml('<div style="background: url('+jsonData.avatar+')"></div>');
    },

    /**
     * Send image to server
     * @param imageURI Image local URI
     * @param comp Component handler
     */
    onCaptureSuccess: function(imageURI, comp) {
        var ft, options, photo_split;

        // Android 4.4 cordova workarounds ... returns new kind or URL for content from chooser
        //if (imageUrl.substring(0,21)=="content://com.android") {
        //http://stackoverflow.com/questions/20638932/unable-to-load-image-when-selected-from-the-gallery-on-android-4-4-kitkat-usin
        if(imageURI.indexOf('content://') != -1 && imageURI.indexOf("%3A") != -1){
            //"PlainFileUrl = content://com.android.providers.media.documents/document/image%3A14",
            photo_split = imageURI.split("%3A");
            imageURI = "content://media/external/images/media/"+photo_split[1];
        }
        // workaround end

        console.log( 'after choose imageURI', imageURI );

        options = new FileUploadOptions();
        // parameter name of file:
        options.fileKey = "file";
        // name of the file:
        options.fileName = imageURI.substr(imageURI.lastIndexOf('/') + 1);

        options.params = {
            accessToken: localStorage.getItem('token')
        };

        ft = new FileTransfer();
        ft.upload(
            imageURI,
            encodeURI(Ksiazece.Config.getHost() + Ksiazece.Config.getSendAvatar()),
            function(data){
                comp.setPhotoAsBackground(data);
            },
            comp.onFail,
            options
        );
    },

    onFail: function(message) {
        Ksiazece.app.getController('Main').alert("Błąd przy wysyłaniu zdjęcia:  " + message);
    }
});