Ext.define('Ksiazece.extension.Message', {
    extend: 'Ext.Component',

    xtype: 'message',

    config: {
        cls: 'message',
        icon: 'caution',
        text: 'Message'
    },

    template: [
        {
            tag: 'span',
            cls: 'icon',
            reference: 'iconElement'
        },
        {
            tag: 'div',
            reference: 'textElement'
        }
    ],

    /**
     * @private
     */
    updateIcon: function(icon) {
        var element = this.iconElement;

        if (element) {
            element.addCls(icon);
            if(icon != 'caution') {
                element.removeCls('caution');
            }
        }
    },

    /**
     * @private
     */
    updateText: function(text) {
        var element = this.textElement;

        if (element) {
            element.setHtml(text);
        }
    }
});