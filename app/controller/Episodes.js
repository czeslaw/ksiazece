Ext.define('Ksiazece.controller.Episodes', {
    extend: 'Ext.app.Controller',

    config: {
        refs: {
            episodesView: 'episodes',
            episodeShareButton: 'episodes [label=fbshare]',
            episodesList: 'episodes list',
            episodeView: 'episode',
            showRankingButton: 'episoderesult button',
            episodeAnswerButton: 'episode button[label=sendAnswer]'
        },
        control: {
            episodesList: {
                itemsingletap: 'showEpisode'
            },
            episodeAnswerButton: {
                tap: 'sendEpisodeAnswer'
            },
            episodesView: {
                activeitemchange: 'toggleShareButton'
            },
            showRankingButton: {
                tap: 'showRanking'
            }
        }
    },

    showRanking: function(){
        var navigation = this.getEpisodesView();

        Ksiazece.app.getController('Main').changeTab('ranking');

        setTimeout(function(){
            navigation.reset();
        }, 0);
    },

    toggleShareButton: function( nav, value, oldValue, eOpts ){
        if(this.getEpisodeShareButton()){
            if(typeof values === 'object' && value.isXType('episode')){
                this.getEpisodeShareButton().show();
            } else {
                this.getEpisodeShareButton().hide();
            }
        }
    },

    onBack: function(){
        var navigation = this.getEpisodesView(),
            view = navigation.getActiveItem();

        if(view.isXType('episoderesult') && view.getResult() == 1) {
            navigation.reset();
        } else {
            navigation.pop();
        }

        return false;
    },

    reload: function(result){
        if(result == '0'){
            this.getEpisodesView().pop();
        }
    },

    initView: function(){
        var store = this.getEpisodesList().getStore();

        store.setParams({
            accessToken: localStorage.getItem('token')
        });

        store.load();
    },

    toggleLoadMoreCmp: function(store){
        var list = this.getEpisodesList(),
            plugin = list.getPlugins()[0];

        if(plugin.storeFullyLoaded()) {
            plugin.getLoadMoreCmp().hide();
        } else {
            plugin.getLoadMoreCmp().show();
        }
    },

    showEpisode: function(list, index, target, record, e, eOpts){
        if(!record.get('soon')){

            if(Ext.Viewport.getMasked() && Ext.Viewport.getMasked().isHidden()){
                Ext.Viewport.mask({xtype: 'loadmask'});
            }

            Ext.data.JsonP.request({
                url: Ksiazece.Config.getHost() + Ksiazece.Config.getEpisodeURL(),
                params: {
                    accessToken: localStorage.getItem('token'),
                    episodeID: record.get('episodeID')
                },
                success: function(response, event){
                    var view;

                    if(response.episode.html || record.get('archive')) {
                        view = Ext.create('Ksiazece.view.episode.Result');

                        view.setResponse( response );
                    } else {
                        view = Ext.create('Ksiazece.view.episode.Episode');

                        view.setItemsData( response.episode );
                        view.setArchive(record.get('archive'));
                    }

                    Ext.Viewport.unmask();

                    this.getEpisodesView().push(view);
                },
                failure: function (response, event) {
                    Ksiazece.app.getController('Main').badConnectionMsg();
                    Ext.Viewport.unmask();
                },
                scope: this
            });
        }
    },

    sendEpisodeAnswer: function(){
        var values = this.getEpisodeView().getValues();

        if(!values.answerID){
            Ksiazece.app.getController('Main').alert("Musisz udzielić odpowiedzi");
            return false;
        }

        if(Ext.Viewport.getMasked() && Ext.Viewport.getMasked().isHidden()){
            Ext.Viewport.mask({xtype: 'loadmask'});
        }

        Ext.data.JsonP.request({
            url: Ksiazece.Config.getHost() + Ksiazece.Config.getSendEpisodeURL(),
            params: {
                accessToken: localStorage.getItem('token'),
                episodeID: values.episodeID,
                answerID: values.answerID
            },
            success: function(response, event){
                var view = Ext.create('Ksiazece.view.episode.Result');

                view.setResponse( response );

                this.getEpisodesView().push(view);

                Ext.Viewport.unmask();
            },
            failure: function (response, event) {
                Ksiazece.app.getController('Main').badConnectionMsg();
                Ext.Viewport.unmask();
            },
            scope: this
        });

        return true;
    }
});