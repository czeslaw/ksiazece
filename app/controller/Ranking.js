Ext.define('Ksiazece.controller.Ranking', {
    extend: 'Ext.app.Controller',

    config: {
        refs: {
            'rankingCardButton': 'ranking segmentedbutton',
            'rankingCards': 'ranking container[label=rankingcards]',
            'localRankingButton': 'ranking button[showCard=1]',
            'user': 'user',
            'list': 'rankingdataview list'
        },
        control: {
            'rankingCardButton': {
                toggle: 'changeCard'
            }
        }
    },

    changeCard: function(segment, button, isPressed ){
        var newCard = button.config.showCard,
            cardsView = this.getRankingCards(),
            oldCard = cardsView.getItems().indexOf( cardsView.getActiveItem()),
            cityID;

        if(isPressed){
            if(newCard == 1 && cardsView.getItems().length == 1) {

                cityID = this.getUser().getUserData().user.city.cityID;

                cardsView.add({
                    xtype: 'rankingdataview',
                    label: 'local',
                    url: Ksiazece.Config.getHost() + Ksiazece.Config.getRankingLocalUrl() + '?cityID='+cityID
                });
            }

            cardsView.animateActiveItem(newCard, {
                type: 'slide',
                direction: (oldCard < newCard)?'left':'right'
            });
        }
    },

    updateCity: function(data){
        this.getLocalRankingButton().setText(data.name);

        if( this.getRankingCards().getItems().length == 2 ){
            this.getRankingCards().query('[label=local]')[0].setUrl(
                Ksiazece.Config.getHost() + Ksiazece.Config.getRankingLocalUrl() + '?cityID='+data.cityID
            );
        }


    },

    initView: function(){
        var cardsView = this.getRankingCards();

        cardsView.getActiveItem().setUrl( Ksiazece.Config.getHost() + Ksiazece.Config.getRankingUrl() );
    },

    toggleLoadMoreCmp: function(){
        var list = this.getRankingCards().getActiveItem().down('list'),
            plugin = list.getPlugins()[0];

        if(plugin.storeFullyLoaded()) {
            plugin.getLoadMoreCmp().hide();
        } else {
            plugin.getLoadMoreCmp().show();
        }
    }
});
