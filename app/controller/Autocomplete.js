Ext.define('Ksiazece.controller.Autocomplete', {
    extend: 'Ext.app.Controller',

    list: null,
    minLetters: 2,

    config: {
        refs: {
            textfield: 'userdata textfield[name=city]'
        },
        control: {
            textfield: {
                focus: 'showList',
                keyup: 'filter',
                change: 'verify',
                clearicontap: 'clearRecord'
            }
        }
    },

    showList: function(field, event){
        var list = Ext.create('Ext.List', {
            itemTpl: '{name}',
            store: 'Cities',
            hideOnMaskTap: true,
            modal: true
        });

        list.on('itemsingletap', this.selectCity, this);

        this.list = list;
    },

    filter: function(field){
        var store = Ext.getStore('Cities');

        if(store.isFiltered()) {
            store.clearFilter();
        }

        if(field.getValue().length == this.minLetters || !store.isLoaded()) {
            store.setParams({
                letter: field.getValue()
            });
            store.load(function(){
                var count = (store.getCount() > 3)?3:store.getCount();

                this.list.setSize('90%', (count*46)+'px');
                this.list.showBy(field, 'tc-bc');
            }, this);
        } else if(field.getValue().length > this.minLetters) {
            store.filter('name', new RegExp('^'+field.getValue(), 'i'));

            var count = (store.getCount() > 3)?3:store.getCount();

            this.list.setSize('90%', (count*46)+'px');
            this.list.showBy(field, 'tc-bc');
        } else {
            this.getTextfield().setRecord();
            this.list.hide();
        }
    },

    selectCity: function(list, index, target, record){
        this.getTextfield().setRecord( record );
        this.getTextfield().setValue( record.get('name') );

        this.list.hide();
    },

    verify: function(field, newValue, oldValue){
        if(newValue == '') {
            this.clearRecord();
        }
    },

    clearRecord: function(){
        console.log( 'icontap' );
        this.getTextfield().setRecord();
    }
});
