Ext.define('Ksiazece.controller.Profile', {
    extend: 'Ext.app.Controller',

    config: {
        refs: {
            userView: 'user',
            profile: 'profile',
            tabs: 'tabs',
            inboxButton: 'user button[label=inbox]'
        },
        control: {
        }
    },

    /**
     * Pobiera dane z API
     * @param force Wymusza pobrane danych mimo, że wczesniej zostały już pobrane
     * @returns {boolean}
     */
    initView: function(force){
        force = (typeof force == 'undefined')?false:force;

        if(this.getProfile().getLoaded() === true && force === false){
            return false;
        }

        if(Ext.Viewport.getMasked() && Ext.Viewport.getMasked().isHidden()){
            Ext.Viewport.mask({xtype: 'loadmask'});
        }

        Ext.data.JsonP.request({
            url: Ksiazece.Config.getHost() + Ksiazece.Config.getUserDataUrl(),
            params: {
                accessToken: localStorage.getItem('token')
            },
            success: function (response, event) {
                if(response.rules == 0) {
                    Ksiazece.app.getController('Account').logout();
                }  else {
                    this.getUserView().setData(response);

                    if(force === false) {
                        this.getProfile().setLoaded(true);
                    }
                }

                Ext.Viewport.unmask();
            },
            failure: function (response, event) {
                Ksiazece.app.getController('Main').badConnectionMsg();
                Ext.Viewport.unmask();
            },
            scope: this
        });

        return true;
    },

    onTipsTap: function(event){
        var listItem = event.target;

        if( event.target.nodeName !== 'LI') {
            listItem = event.target.parentNode;
        }

        Ksiazece.app.getController('Main').changeTab(listItem.getAttribute('data-view'));
    },

    changeAvatar: function(){
        this.getUserView().setData()
    },

    setInboxBadge: function( text ){
        this.getInboxButton().setBadgeText( text );
    }
});
