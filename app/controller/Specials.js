Ext.define('Ksiazece.controller.Specials', {
    extend: 'Ext.app.Controller',

    config: {
        quizStep: 0,

        refs: {
            specialsView: 'specials',
            jokeView: 'specialsjoke',
            jokeTextArea: 'specialsjoke textareafield',
            sendJokeButton: 'specialsjoke button[label=send]',
            quizNextStep: 'specials button[label=nextStep]'
        },
        control: {
            sendJokeButton: {
                tap: 'sendJoke'
            },
            quizNextStep: {
                tap: 'quizNextStep'
            },
            specials: {
                pop: function(){
                    this.setQuizStep( this.getQuizStep() - 1 );
                }
            },
            jokeTextArea: {
                keyup: 'countChars'
            }
        }
    },

    /**
     * Pobiera dane zadania z API i inicjuje wyswietlenie odpowiedniego widoku
     * @returns {boolean}
     */
    initView: function(){
        if(Ext.Viewport.getMasked() && Ext.Viewport.getMasked().isHidden()){
            Ext.Viewport.mask({xtype: 'loadmask'});
        }

        Ext.data.JsonP.request({
            url: Ksiazece.Config.getHost() + Ksiazece.Config.getSpecialsUrl(),
            params: {
                accessToken: localStorage.getItem('token')
            },
            success: function (response, event) {
                if(response.authorisation == 0) {
                    Ksiazece.app.getController('Account').logout();
                }  else {
                    this.prepareItem( response.specials );
                }

                Ext.Viewport.unmask();
            },
            failure: function (response, event) {
                Ksiazece.app.getController('Main').badConnectionMsg();
                Ext.Viewport.unmask();
            },
            scope: this
        });

        return false;
    },

    /**
     * Liczy znaki i wyświetla limit
     * @param textarea
     * @param e
     * @param eOpts
     */
    countChars: function( textarea,  e, eOpts ) {
        var counterPanel = this.getJokeView().query('[label=counter]')[0];

        counterPanel.setData({ charsLeft: 360 - textarea.getValue().length } );
    },

    prepareItem: function(response){
        var specials = this.getSpecialsView(),
            view;

        switch(response.type){
            case 'message':
                view = Ext.create('Ksiazece.view.specials.Message');
                break;
            case 'joke':
                view = Ext.create('Ksiazece.view.specials.Joke');
                break;
            case 'photo':
                view = Ext.create('Ksiazece.view.specials.Photo');
                break;
            case 'quiz':
                Ksiazece.Config.setQuizData(response);
                this.quizNextStep();
                break;
            case 'results':
                view = Ext.create('Ksiazece.view.specials.Results');
                break;
            default:
                view = Ext.create('Ksiazece.view.specials.Message');
                response.message = {
                    text: 'Nie wiem czego serwer chce ode mnie.'
                };
                break;
        }

        if(view && specials.getItems().length == 1) {
            view.setItemsData( response );
            specials.add(view);
        }
    },

    /**
     * Wysyła odpowiedź
     * @param values
     * @private
     */
    _sendAnswer: function(values){
        if(Ext.Viewport.getMasked() && Ext.Viewport.getMasked().isHidden()){
            Ext.Viewport.mask({xtype: 'loadmask'});
        }

        Ext.data.JsonP.request({
            url: Ksiazece.Config.getHost() + Ksiazece.Config.getSpecialSendStep(),
            params: Ext.Object.merge({}, {accessToken: localStorage.getItem('token')}, values),
            success: function (response, event) {
                if(response.rules == 0) {
                    Ksiazece.app.getController('Account').logout();
                }

                var view = Ext.create('Ksiazece.view.specials.ThankYou');

                response.type = 'joke';

                view.setItemsData( response );

                this.getSpecialsView().add(view);

                Ext.Viewport.unmask();
            },
            failure: function (response, event) {
                Ksiazece.app.getController('Main').badConnectionMsg();
                Ext.Viewport.unmask();
            },
            scope: this
        });
    },

    sendJoke: function(){
        var values = this.getJokeView().getValues();

        if(values.answer == ''){
            Ksiazece.app.getController('Main').alert("Musisz wpisać odpowiedź");
        } else {
            this._sendAnswer(values);
        }
    },

    /**
     * Iteruje przez kolejne pytania w zadaniu typu 'quiz' (ankieta)
     * @returns {boolean}
     */
    quizNextStep: function(){
        var response = Ksiazece.Config.getQuizData(),
            step = this.getQuizStep(),
            currentQuestion = this.getSpecialsView().getActiveItem(),
            view;

        if(step == response.data.steps.length){
            view = Ext.create('Ksiazece.view.specials.ThankYou');
            view.setItemsData( {
                specialMessage: {
                    text: 'Dziękujemy za wypełnienie ankiety'
                },
                type: 'quiz'
            } );
        } else {
            if(response.data.steps[step].type == 1) {
                view = Ext.create('Ksiazece.view.specials.quiz.Text');
            } else if(response.data.steps[step].type == 2) {
                view = Ext.create('Ksiazece.view.specials.quiz.Quiz');
            }
            view.setStep( step );
            view.setItemsData( response );
        }

        if( currentQuestion ){
            if( !currentQuestion.isValid() ) {
                Ksiazece.app.getController('Main').alert('Odpowiedź nie może być pusta.');
                return false;
            }

            if(Ext.Viewport.getMasked() && Ext.Viewport.getMasked().isHidden()){
                Ext.Viewport.mask({xtype: 'loadmask'});
            }

            Ext.data.JsonP.request({
                url: Ksiazece.Config.getHost() + Ksiazece.Config.getSpecialSendStep(),
                params: Ext.Object.merge({}, {accessToken: localStorage.getItem('token'), type: 'quiz'}, currentQuestion.getValues()),
                success: function (response, event) {
                    if(response.authorisation == 0) {
                        Ksiazece.app.getController('Account').logout();
                    }

                    Ext.Viewport.unmask();
                },
                failure: function (response, event) {
                    Ksiazece.app.getController('Main').badConnectionMsg();
                    Ext.Viewport.unmask();
                },
                scope: this
            });
        }

        this.setQuizStep( step + 1 );
        this.getSpecialsView().add(view);

        return true;
    }
});
