Ext.define('Ksiazece.controller.ResetPassword', {
    extend: 'Ext.app.Controller',

    config: {
        refs: {
            resetForm: 'remind button',
            field: 'remind field'
        },
        control: {
            resetForm: {
                tap: 'sendResetRequest'
            }
        }
    },

    sendResetRequest: function(){
        var field = this.getField();

        console.log( field );
        console.log( field.getValue(), Ksiazece.app.getController('Share')._validateEmail(field.getValue()) );

        if(!Ksiazece.app.getController('Share')._validateEmail(field.getValue())) {
            field.getParent().setStatus({
                validated: 0,
                validatedMessages: 'Musisz podać prawidłowy adres e-mail'
            });
        } else {
            field.getParent().setStatus({
                validated: 1
            });

            //TODO: wyslac ajaxa
        }
    }

});
