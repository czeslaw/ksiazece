Ext.define('Ksiazece.controller.Account', {
    extend: 'Ext.app.Controller',

    config: {
        FBData: {},

        refs: {
            'mainView': 'main',
            'loginView': 'login',
            'loginForm': 'login formpanel',
            'loginButton': 'login button[label=login]',
            'showAgeVerificationButton': 'login button[label=register]',
            'facebookButton': 'login button[ui=facebook]',
            'ageVerificationView': 'register',
            'ageVerificationForm': 'register formpanel',
            'ageVerificationFields': 'register field',
            'showUserDataButton': 'register button[label=verify-age]',
            'userData': 'userdata ',
            'userDataCardButton': 'userdata segmentedbutton',
            'showNextCardButton': 'userdata button[label=showNextCard]',
            'userDataCards': 'userdata container[label=userdatacards]',
            'showTermsButton': 'login userdata button[label=showTerms]',
            'editUserData': 'profile userdata button[label=showTerms]',
            'acceptTermsButton': 'terms button[label=acceptTerms]',
            'declineTermsButton': 'terms button[label=declineTerms]',
            'profileView': 'profile',
            'showEditProfileButton': 'profile button[showView=UserData]',
            'logoutButton': 'profile button[showView=Login]',
            'userDataInputs': 'userdata field',
            'sendUserDataButton': '[label=pubData] button',
            'userView': 'profile user',
            'showMessagesButton': 'profile button[showView=UserMessages]',
            'messageList': 'usermessages list',
            'newCityForm': 'formpanel[label=sendNewCity]',
            'sendNewCityButton': 'formpanel[label=sendNewCity] button'
        },
        control: {
            'userDataCardButton': {
                toggle: 'changeUserDataCard'
            },
            'loginButton': {
                tap: 'login'
            },
            'showAgeVerificationButton': {
                tap: 'verifyAge'
            },
            showUserDataButton: {
                tap: 'showUserData'
            },
            showNextCardButton: {
                tap: 'showNextUserCard'
            },
            showTermsButton: {
                tap: 'showTerms'
            },
            acceptTermsButton: {
                tap: 'acceptTerms'
            },
            declineTermsButton: {
                tap: 'declineTerms'
            },
            showEditProfileButton: {
                tap: 'showEditProfile'
            },
            profileView: {
                pop: 'showButtons'
            },
            userDataInputs: {
                change: 'validateField'
            },
            editUserData: {
                tap: 'edit'
            },
            logoutButton: {
                tap: 'logout'
            },
            sendUserDataButton: {
                tap: 'register'
            },
            facebookButton: {
                tap: 'facebookLogin'
            },
		    showMessagesButton: {
			    tap: 'showMessages'
		    },
            messageList: {
                itemsingletap: 'changeTab'
            },
            ageVerificationFields: {
                keyup: 'limitChars'
            },
            sendNewCityButton: {
                tap: 'sendNewCity'
            }
        }
    },

    limitChars: function(field, e, eOpts){
        var value = field.getValue(),
            maxLength =  field.getMaxLength();

        if (value && (value.toString().length > maxLength)){
            field.setValue(value.toString().substring(0, maxLength));
            return false;
        }

        return true;
    },

    _loginSuccess:  function(response, event){
        var mainController = Ksiazece.app.getController('Main'),
            loginForm = this.getLoginForm(),
            emailField = loginForm.query('[name=email]')[0],
            passwordField = loginForm.query('[name=password]')[0];

        Ext.Viewport.unmask();

        if(response.login == 'ok') {
            localStorage.setItem('logged', true);
            localStorage.setItem('token', response.accessToken);
            localStorage.setItem('userType', response.userType);

            emailField.getParent().setStatus({ validated: 1 });
            passwordField.getParent().setStatus({ validated: 1 });

            mainController.initTabs();
        } else {
            emailField.getParent().setValid(false);
            passwordField.getParent().setStatus({
                validated: 0,
                validatedMessages: response.login
            });
        }
    },

    /**
     * Loguje uzytkownika, zapisuje accessToken
     */
    login: function(){
        var loginForm = this.getLoginForm(),
            emailField = loginForm.query('[name=email]')[0],
            passwordField = loginForm.query('[name=password]')[0],
            loginData = loginForm.getValues();

        if(loginData.email != '' && loginData.pass != '') {
            if(Ext.Viewport.getMasked() && Ext.Viewport.getMasked().isHidden()){
                Ext.Viewport.mask({xtype: 'loadmask'});
            }

            Ext.data.JsonP.request({
                url: Ksiazece.Config.getHost() + Ksiazece.Config.getLoginUrl(),
                params: Ext.Object.merge({}, {pushToken: localStorage.getItem('pushToken')}, loginData),
                success: this._loginSuccess,
                failure: function (response, event) {
                    Ksiazece.app.getController('Main').alert("E-mail albo hasło nie są poprawne");
                    Ksiazece.app.getController('Account').logout();
                    Ext.Viewport.unmask();
                },
                scope: this
            });
        } else {
            emailField.getParent().setValid(false);
            passwordField.getParent().setStatus({
                validated: 0,
                validatedMessages: "Nie podałeś danych."
            });
        }
    },

    facebookLogin: function(){
        var that = this;

        FB.login(function(response) {
            if (response.authResponse) {
                FB.api('/me', function(user) {
                    //try to login
                    if(Ext.Viewport.getMasked() && Ext.Viewport.getMasked().isHidden()){
                        Ext.Viewport.mask({xtype: 'loadmask'});
                    }

                    var data = {
                        fbUserID: user.id,
                        avatar: 'http://graph.facebook.com/'+user.id+'/picture'
                    };

                    console.log( user, data );

                    Ext.data.JsonP.request({
                        url:  Ksiazece.Config.getHost() + Ksiazece.Config.getLoginUrl(),
                        params: {
                            email: user.email,
                            fbUserId: user.id
                        },
                        success: this._loginSuccess,
                        failure: function (response, event) {
                            Ext.Viewport.unmask();

                            this.setFBData({
                                fbUserID: user.id,
                                avatar: 'http://graph.facebook.com/'+user.id+'/picture',
                                email: user.email,
                                emailConfirm: user.email,
                                name: user.first_name,
                                surname: user.last_name
                            });

                            this.verifyAge();
                        },
                        scope: that
                    });
                });
            } else {
                console.log('User cancelled login or did not fully authorize.');
            }
        }, {scope: "email"});

    },

    /**
     * Wylogowuje uzytkownika, usuwa accessToken z localStorage
     */
    logout: function(){
        localStorage.removeItem('token');
        localStorage.removeItem('userType');
        localStorage.setItem('logged', false);

        this.getMainView().animateActiveItem(0, {
            type: 'slide',
            direction: 'right'
        });
    },

    /**
     * Walidacja pola po blur
     * @param field
     * @param newValue
     * @param oldValue
     */
    validateField: function(field, newValue, oldValue){
        var value = field.getValue();

        if(field.getDisabled()) {
            return false;
        }

        if(field.getName() == 'city' && field.getRecord()) {
            value = field.getRecord().get('cityID');
        }

        Ext.data.JsonP.request({
            url: Ksiazece.Config.getHost() + Ksiazece.Config.getValidateUrl(),
            params: {
                name: field.getName(),
                value: value
            },
            success: function(response, event){
                field.getParent().setStatus(response);
                field.setData(response);

                if(field.getName() == 'emailConfirm' || field.getName() == 'email'){
                    this.checkEmail(response);
                }

            },
            failure: function (response, event) {
                Ksiazece.app.getController('Main').badConnectionMsg();
            },
            scope: this
        });

        return true;
    },

    /**
     * Sprawdza czy podane adresy są identyczne
     * @param response
     */
    checkEmail: function(response){
        var emailField = this.getUserData().down('field[name=email]'),
            emailConfirmField = this.getUserData().down('field[name=emailConfirm]');

        if(emailConfirmField.getValue() != emailField.getValue()) {
            response = {
                "validated": 0,
                "validatedMessages": "Adresy powinny być identyczne"
            };
        }

        emailConfirmField.getParent().setStatus(response);
        emailConfirmField.setData(response);
    },

    /**
     * Sprawdzenie czy wszystkie pola w danym formularzu sa poprawne
     * @param form
     * @returns {boolean}
     */
    checkValidation: function(form){
        var fields = form.query('field'),
            fieldsNumber = fields.length,
            data = {},
            validForm = true;

        for(var i=0; i < fieldsNumber; i++){
            if(fields[i].getDisabled() || fields[i].getHidden() || (this.getUserData().getMode() == 'edit' && fields[i].getName() == 'password')){
                continue;
            }

            if(fields[i].getData()) {
                if(fields[i].getData().validated == 0) {
                    data = fields[i].getData();
                }
            } else {
                data = {
                    "validated": 0,
                    "validatedMessages": 'Nie wypełnione pole '+fields[i].getPlaceHolder()
                };
            }

            if(data.validated == 0) {
                fields[i].getParent().setStatus(data);
                validForm = false;
            }

            data = {};
        }

        return validForm;
    },

    /**
     * Rejestracja uzytkownika. Zapisuje accessToken i userType w localStorage.
     * Jeżeli istnieje już token w localStorgae zwraca false.
     * @returns {boolean}
     */
    register: function(){
        var registerData = this.getUserData().getValues();

        if(localStorage.getItem('token')){
            return false;
        }

        if(Ext.Viewport.getMasked() && Ext.Viewport.getMasked().isHidden()){
            Ext.Viewport.mask({xtype: 'loadmask'});
        }

        Ext.data.JsonP.request({
            url: Ksiazece.Config.getHost() + Ksiazece.Config.getRegisterUrl(),
            params: Ext.Object.merge({}, {pushToken: localStorage.getItem('pushToken')}, registerData, Ksiazece.Config.getBirthDate()),
            success: function(response, event){
                if(response.status == 'ok') {
                    localStorage.setItem('logged', true);
                    localStorage.setItem('token', response.accessToken);
                    localStorage.setItem('userType', registerData.userType);

                    this.getUserData().enablePhotoTab();
                    this.getUserDataCardButton().setPressedButtons([2]);
                } else {
                    Ksiazece.app.getController('Main').alert(response.login);
                }

                Ext.Viewport.unmask();
            },
            failure: function (response, event) {
                var mainController = Ksiazece.app.getController('Main');

                mainController.alert("E-mail albo hasło nie są poprawne");
                mainController.logout();

                Ext.Viewport.unmask();
            },
            scope: this
        });

        return true;
    },

    /**
     * Wysyła potwierdzenie zaakceptowania regulaminu
     */
    acceptTerms: function(){
        if(Ext.Viewport.getMasked() && Ext.Viewport.getMasked().isHidden()){
            Ext.Viewport.mask({xtype: 'loadmask'});
        }

        Ext.data.JsonP.request({
            url: Ksiazece.Config.getHost() + Ksiazece.Config.getAcceptRulesUrl(),
            params: {
                accessToken: localStorage.getItem('token')
            },
            success: function(response, event){
                this.resetLoginView();
                Ksiazece.app.getController('Main').initTabs();

                Ext.Viewport.unmask();
            },
            failure: function (response, event) {
                Ksiazece.app.getController('Main').alert("E-mail albo hasło nie są poprawne");
                Ksiazece.app.getController('Account').logout();

                Ext.Viewport.unmask();
            },
            scope: this
        });
    },

    declineTerms: function(){
        var that = this;

         Ksiazece.app.getController('Main').confirm("Czy jesteś pewien?", function(){
             Ext.data.JsonP.request({
                 url: Ksiazece.Config.getHost() + Ksiazece.Config.getRulesNotAcceptedUrl(),
                 params: {
                     accessToken: localStorage.getItem('token')
                 },
                 success: function(response, event){
                     navigator.app.exitApp();
                     that.logout();
                 },
                 failure: function (response, event) {
                     that.logout();
                 },
                 scope: this
             });
         });
    },

    /**
     * Zapisanie zmienionych danych
     */
    edit: function(){
        var values = this.getUserData().getValues();

        if(Ext.Viewport.getMasked() && Ext.Viewport.getMasked().isHidden()){
            Ext.Viewport.mask({xtype: 'loadmask'});
        }

        Ext.data.JsonP.request({
            url: Ksiazece.Config.getHost() + Ksiazece.Config.getEditUserUrl(),
            params: Ext.Object.merge({}, {'accessToken': localStorage.getItem('token')}, values),
            success: function(response, event){

                values.city = this.getUserData().query('field[name=city]')[0].getRecord().getData();

                this.getUserView().setData({user: values, successMessage: response.editMessage});
                this.getProfileView().setLoaded(true);

                if(response.edit == '1') {
                    this.getProfileView().pop();
                }

                Ext.Viewport.unmask();
            },
            failure: function (response, event) {
                Ksiazece.app.getController('Main').badConnectionMsg();
                Ext.Viewport.unmask();
            },
            scope: this
        });
    },

    /**
     * Pokazuje widok sprawdzania wieku
     */
    verifyAge: function(){
        var view = Ext.create('Ksiazece.view.account.Register');

        this.getLoginView().push(view);
    },

    /**
     * Pokazuje widok przypominania hasła
     */
    showRemindForm: function(){
        var view = Ext.create('Ksiazece.view.account.Remind');

        this.getLoginView().push(view);
    },

    /**
     * Sprawdza czy uzytkownik jest pełnoletni - jezeli tak pokazuje formularz rejestracji
     */
    showUserData: function(){
        var message = this.getAgeVerificationView().query('[label=message]')[0],
            form = this.getAgeVerificationForm(),
            birthDate = form.getValues(),
            now = new Date(),
            month = now.getMonth() + 1,
            day = now.getDate(),
            years = now.getFullYear() - birthDate.year,
            view;

        if(Ext.Date.isValid(birthDate.year, birthDate.month, birthDate.day) && years > 0) {
            if (month < birthDate.month || (month == birthDate.month && day < birthDate.day)) {
                years -= 1;
            }

            if(birthDate.year && years >= 18 && years < 100) {
                Ksiazece.Config.setBirthDate( birthDate );

                view = Ext.create('Ksiazece.view.account.UserData');

                if(this.getFBData().fbUserID){
                    view.setMode('facebook');
                    view.setFormValues( this.getFBData() );
                }

                this.getLoginView().push(view);
            } else if (years >= 100 && years < 200) {
                message.setData({msg: 'Gratulacje, jesteś najstarszym człowiekiem na świecie ale za barem już raczej nie staniesz.'});
            } else if(years < 18) {
                message.setData({msg: 'Przykro nam, ale jedynie osoby pełnoletnie mogą korzystać z naszej aplikacji. Zapraszamy na stronę aby dowiedzieć się węcej <a href="#" onclick="window.open(\'http://www.niedlanieletnich.pl\', \'_system\');">www.niedlanieletnich.pl</a>'});
            }
        } else {
            message.setData({msg: 'Podaj poprawną datę urodzenia.'});
        }
    },

    /**
     * Zmienia karty formularzu, w panelu rejestracji (przyciski na górze)
     * @param segment
     * @param button
     * @param isPressed
     */
    changeUserDataCard: function(segment, button, isPressed ){
        var newCard = button.config.showCard,
            cardsView = this.getUserDataCards(),
            form = cardsView.getActiveItem(),
            oldCard = cardsView.getItems().indexOf( form);

        if(isPressed && newCard != oldCard){
            if(this.checkValidation(cardsView.getActiveItem())) {
                cardsView.animateActiveItem(newCard, {
                    type: 'slide',
                    direction: (oldCard < newCard)?'left':'right'
                });
            } else {
                this.getUserDataCardButton().setPressedButtons([oldCard]);
            }
        }
    },

    /**
     * Zmienia karty formularza, w panelu rejestracji (przyciski na dole).
     * Wywołuje Ksiazece.controller.Account.changeUserDataCard() (setPressedButton wywoluje zdarzenie na gornych przyciskach)
     */
    showNextUserCard: function(){
        var cardsView = this.getUserDataCards(),
            oldCard = cardsView.getItems().indexOf( cardsView.getActiveItem() ),
            newCard = (oldCard == 2)?2:oldCard+1;

        if(this.checkValidation(cardsView.getActiveItem()) && !this.getUserDataCardButton().getAt(newCard).getDisabled()) {
            this.getUserDataCardButton().setPressedButtons([newCard]);
        }
    },

    /**
     * Pokazuje widok z regulaminem
     */
    showTerms: function(){
        var view = Ext.create('Ksiazece.view.account.Terms');

        this.getLoginView().push(view);
    },

    /**
     * Wraca do widoku rejestracji z dowolnego kroku rejestracji
     */
    resetLoginView: function(){
        this.getLoginView().reset();
    },

    /**
     * Pokazuje formularz edycji danych uzytkownika
     * @param button
     */
    showEditProfile: function(button){
        if(Ext.Viewport.getMasked() && Ext.Viewport.getMasked().isHidden()){
            Ext.Viewport.mask({xtype: 'loadmask'});
        }

        Ext.data.JsonP.request({
            url: Ksiazece.Config.getHost() + Ksiazece.Config.getUserDataUrl(),
            params: {
                accessToken: localStorage.getItem('token')
            },
            success: function(response, event){
                var view = Ext.create('Ksiazece.view.account.UserData');

                view.setMode('edit');
                view.setFormValues(response.user);

                view.query('addphoto')[0].setPhotoAsBackground(response.user);

                this.getProfileView().push(view);
                this.getLogoutButton().hide();
                button.hide();

                Ext.Viewport.unmask();
            },
            failure: function (response, event) {
                Ksiazece.app.getController('Main').badConnectionMsg();
                Ext.Viewport.unmask();
            },
            scope: this
        });
    },

    /**
     * Pokazuje przycisk "Edycja" w profilu uzytkownika po zakończeniu edycji
     */
    showButtons: function(){
        this.getShowEditProfileButton().show();
        this.getLogoutButton().show();
        if(this.getShowMessagesButton()) {
            this.getShowMessagesButton().show();
        }
    },

    showMessages: function(button){
        if(Ext.Viewport.getMasked() && Ext.Viewport.getMasked().isHidden()){
            Ext.Viewport.mask({xtype: 'loadmask'});
        }

        Ext.data.JsonP.request({
            url: Ksiazece.Config.getHost() + Ksiazece.Config.getMessageListUrl(),
            params: {
                accessToken: localStorage.getItem('token')
            },
            success: function(response, event){
                var view = Ext.create('Ksiazece.view.account.UserMessages');

                var store = this.getMessageList().getStore();

                store.setParams({
                    accessToken: localStorage.getItem('token')
                });

                store.load();

                this.getProfileView().push(view);
                this.getLogoutButton().hide();
                this.getShowEditProfileButton().hide();
                button.hide();


                Ksiazece.app.getController('Profile').setInboxBadge('');

                Ext.Viewport.unmask();
            },
            failure: function (response, event) {
                Ksiazece.app.getController('Main').badConnectionMsg();
                Ext.Viewport.unmask();
            },
            scope: this
        });
    },

    changeTab: function(list, index, target, record, e, eOpts){
        var view = record.get('showView');

        if(view) {
            Ksiazece.app.getController('Main').changeTab( view );
        }
    },

    showNewCityForm: function(){
        Ext.Viewport.add({
            xtype: 'formpanel',
            modal: true,
            hideOnMaskTap: true,
            label: 'sendNewCity',
            width: '90%',
            height: '220px',
            items: [
                {
                    xtype: 'fieldset',
                    items: [
                        {
                            xtype: 'formfield',
                            fieldConfig: {
                                xtype: 'emailfield',
                                name: 'email',
                                label: false,
                                placeHolder: 'Twój email'
                            }
                        },
                        {
                            xtype: 'formfield',
                            fieldConfig: {
                                xtype: 'textfield',
                                name: 'city',
                                label: false,
                                placeHolder: 'Miasto'
                            }
                        }
                    ]
                },
                {
                    xtype: 'button',
                    label: 'sendNewCity',
                    text: 'Wyślij',
                    ui: 'light',
                    margin: 10
                }
            ],
            centered: true
        });
    },

    sendNewCity: function(){
        var values = this.getNewCityForm().getValues(),
            emailField = this.getNewCityForm().down('field[name=email]'),
            cityField = this.getNewCityForm().down('field[name=city]'),
            valid = false;

        if(!Ksiazece.app.getController('Share')._validateEmail(values.email)) {
            emailField.getParent().setStatus({
                validated: 0,
                validatedMessages: 'Musisz podać prawidłowy adres e-mail'
            });
            valid = false;
        } else {
            emailField.getParent().setStatus({
                validated: 1
            });

            valid = true;
        }

        if(!values.city){
            cityField.getParent().setStatus({
                validated: 0,
                validatedMessages: 'Musisz podać miasto'
            });

            valid = false;
        } else {
            cityField.getParent().setStatus({
                validated: 1
            });

            valid = true;
        }

        if(valid === true) {

            Ext.data.JsonP.request({
                url: Ksiazece.Config.getHost() + Ksiazece.Config.getSendCityUrl(),
                params: values,
                success: function(response, event){
                    Ksiazece.app.getController('Main')._globalMessage({
                        text: response.specialMessage
                    });
                },
                failure: function (response, event) {
                    Ksiazece.app.getController('Main').badConnectionMsg();
                },
                scope: this
            });

            this.getNewCityForm().getParent().remove(this.getNewCityForm());
        } else {
            this.getNewCityForm().setHeight('255px');
        }
    }
});
