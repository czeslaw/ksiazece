Ext.define('Ksiazece.controller.Share', {
    extend: 'Ext.app.Controller',

    config: {
        refs: {
            fbButton: 'button[label=fbshare]',
            emailButton: 'button[label=emailshare]',
            saveButton: 'button[label=save]',
            sendEmailButton: 'button[label=sendShareEmail]',
            emailForm: 'formpanel[label=sendShareEmail]'
        },
        control: {
            fbButton: {
                tap: 'shareOnFacebook'
            },
            emailButton: {
                tap: 'showEmailForm'
            },
            saveButton: {
                tap: 'downloadFile'
            },
            sendEmailButton: {
                tap: 'sendEmail'
            }
        }
    },

    shareOnFacebook: function(button){
        var data = Ext.ComponentQuery.query(button.config.parentXType)[0].getShareData();

        window.plugins.socialsharing.shareViaFacebook(
            data.message,
            null,
            data.url,
            function() {
                console.log('share ok');
            }, function(errormsg){
                console.log(errormsg);
            });
    },

    _validateEmail: function (email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    },

    showEmailForm: function(button){
        var parentView = Ext.ComponentQuery.query(button.config.parentXType)[0],
            data = parentView.getShareData();

        Ext.Viewport.add({
            xtype: 'formpanel',
            modal: true,
            hideOnMaskTap: true,
            label: 'sendShareEmail',
            width: '90%',
            height: '270px',
            items: [
                {
                    xtype: 'fieldset',
                    items: [
                        {
                            xtype: 'hiddenfield',
                            value: button.config.parentXType,
                            name: 'parentXType'
                        },
                        {
                            xtype: 'formfield',
                            fieldConfig: {
                                xtype: 'textfield',
                                name: 'emails',
                                label: false,
                                placeHolder: 'Adresy oddzielone przecinkiem'
                            }
                        },
                        {
                            xtype: 'textareafield',
                            name: 'body',
                            label: false,
                            value: data.message + ": " + data.url
                        }
                    ]
                },
                {
                    xtype: 'button',
                    label: 'sendShareEmail',
                    text: 'Wyślij',
                    ui: 'light',
                    margin: 10
                }
            ],
            centered: true
        });

    },

    /**
     *
     * @param button
     */
    sendEmail: function(button){
        var data = this.getEmailForm().getValues(),
            emailsField = this.getEmailForm().query('textfield[name=emails]')[0],
            emailsString = data.emails,
            emailsArray = [],
            valid = true,
            parentView =  Ext.ComponentQuery.query(data.parentXType)[0],
            url;

        if(emailsString) {
            emailsArray = emailsString.split(',', 5);

            for (var i = 0; i < emailsArray.length; i++) {
                if(!this._validateEmail(emailsArray[i].trim())) {
                    valid = false;
                }
            }

            emailsField.getParent().setStatus({
                validated: valid?1:0,
                validatedMessages: 'Nieprawidłowe adresy'
            });
        } else {
            emailsField.getParent().setStatus({
                validated: 0,
                validatedMessages: 'Musisz podać adresy'
            });

            valid = false;
        }

        if(valid === true) {
            switch (data.parentXType){
                case 'generatorpreview':
                    data.memID = parentView.getShareData().memID;
                    url = Ksiazece.Config.getHost() + Ksiazece.Config.getShareMemUrl();
                    break;
                case 'episode':
                    data.episodeID = parentView.getShareData().episodeID;
                    url = Ksiazece.Config.getHost() + Ksiazece.Config.getShareEpisodeUrl();
                    break;
            }

            if(url){
                Ext.data.JsonP.request({
                    url: url,
                    params: Ext.Object.merge({}, {accessToken: localStorage.getItem('token')}, data),
                    success: function(response, event) {
                        if (response.rules == 0) {
                            Ksiazece.app.getController('Account').logout();
                        }

                        if(response.specialMessage){
                            Ksiazece.app.getController('Main')._globalMessage({
                                text: response.specialMessage
                            });
                        }

                        console.log( response );
                    },
                    failure: function(response, event) {
                        Ksiazece.app.getController('Main').badConnectionMsg();
                    },
                    scope: this
                });
            }

            this.getEmailForm().getParent().remove(this.getEmailForm());
        }
    },

    /**
     * Zapisuje mem do galerii
     * @param button
     */
    downloadFile: function(button){
        var that = this,
            parentView = Ext.ComponentQuery.query(button.config.parentXType)[0],
            data = parentView.getShareData(),
            can = document.getElementById('canvas'),
            ctx = can.getContext('2d'),
            img = new Image();

        img.onload = function(){
            can.width = img.width;
            can.height = img.height;
            ctx.drawImage(img, 0, 0, img.width, img.height);

            window.canvas2ImagePlugin.saveImageDataToLibrary(
                function(msg){
                    var message = parentView.query('message')[0];

                    message.show();

                    setTimeout(function(){
                        message.hide();
                    }, 3000);
                },
                function(err){
                    console.log(err);
                },
                document.getElementById('canvas')
            );
        };

        img.src = data.url
    }
});
