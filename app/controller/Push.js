Ext.define('Ksiazece.controller.Push', {
    extend: 'Ext.app.Controller',

    config: {
        refs: {
            'tabsView': 'tabs'
        }
    },

    initAndroidPushwoosh: function () {

        if(typeof window.plugins !== 'undefined' && typeof window.plugins.pushNotification !== 'undefined') {
            var pushNotification = window.plugins.pushNotification;

            //set push notifications handler
            document.addEventListener('push-notification', function(event) {
                var title = event.notification.title;
                var userData = event.notification.userdata;

                console.log( event );

                var tabsMap = {
                    episode: 0,
                    specials: 1,
                    ranking: 2,
                    generator: 3,
                    profile: 4
                };

                if(typeof(userData) != "undefined") {
                    console.warn('user data: ' + tabsMap[userData.showView]);

                    this.getTabsView().setActiveItem(tabsMap[userData.showView]);
                }

                //alert(title);
            });

//            console.log('initialize Pushwoosh');
            //initialize Pushwoosh with projectid: "GOOGLE_PROJECT_ID", appid : "PUSHWOOSH_APP_ID". This will trigger all pending push notifications on start.
//            pushNotification.onDeviceReady({ projectid: "742721331070", appid : "1CA1D-D14FB" });

//            console.log('try register device');
            //register for pushes
            var regResp = pushNotification.registerDevice(
                { projectid: "742721331070", appid : "1CA1D-D14FB" },
                function(status) {
//                    console.log('token', status);
//                    console.warn('push token: ' + status);
                    localStorage.setItem('pushToken', status);

                    Ext.data.JsonP.request({
                        url: Ksiazece.Config.getHost() + Ksiazece.Config.getSendPushToken(),
                        params: {
                            accessToken: localStorage.getItem('token'),
                            deviceToken: status
                        },
                        success: function (response, event) {
                            if(response.authorisation == 0) {
                                Ksiazece.app.getController('Account').logout();
                            }
                        },
                        failure: function (response, event) {
                            Ksiazece.app.getController('Main').badConnectionMsg();
                        },
                        scope: this
                    });

                },
                function(status) {
//                    console.log('token failed');
//                    console.warn(JSON.stringify(['failed to register ', status]));
                }
            );

            console.log(regResp);
        }
    },

    initIOSPushwoosh: function() {
        var pushNotificatiospran = window.plugins.pushNotification;
        this.registerPushwooshIOS();
        pushNotification.onDeviceReady();
    },

    // the token has been registered, we can use Pushwoosh plugin methods
    onPushwooshiOSInitialized: function (pushToken) {
        var pushNotification = window.plugins.pushNotification;
        //retrieve the tags for the device
        pushNotification.getTags(function(tags) {
                console.warn('tags for the device: ' + JSON.stringify(tags));
            },
            function(error) {
                console.warn('get tags error: ' + JSON.stringify(error));
            });

        //start geo tracking. PWTrackSignificantLocationChanges - Uses GPS in foreground, Cell Triangulation in background.
//        pushNotification.startLocationTracking('PWTrackSignificantLocationChanges',
//            function() {
//                console.warn('Location Tracking Started');
//            });
    },

    registerPushwooshIOS: function() {
        var pushNotification = window.plugins.pushNotification;

        //push notifications handler
        document.addEventListener('push-notification', function(event) {
            var title = event.notification.title;
            var userData = event.notification.userdata;

            console.log( event );

            var tabsMap = {
                episode: 0,
                specials: 1,
                ranking: 2,
                generator: 3,
                profile: 4
            };

            if(typeof(userData) != "undefined") {
                console.warn('user data: ' + tabsMap[userData.showView]);

                this.getTabsView().setActiveItem(tabsMap[userData.showView]);
            }
        });

        //register for push notifications
        pushNotification.registerDevice({alert:true, badge:true, sound:true, pw_appid:"1CA1D-D14FB", appname:"Barmanska"},
            function(status) {
                //this is a push token
                var deviceToken = status['deviceToken'];

                localStorage.setItem('pushToken', deviceToken);

                Ext.data.JsonP.request({
                    url: Ksiazece.Config.getHost() + Ksiazece.Config.getSendPushToken(),
                    params: {
                        accessToken: localStorage.getItem('token'),
                        deviceToken: deviceToken
                    },
                    success: function (response, event) {
                        if(response.authorisation == 0) {
                            Ksiazece.app.getController('Account').logout();
                        }
                    },
                    failure: function (response, event) {
                        Ksiazece.app.getController('Main').badConnectionMsg();
                    },
                    scope: this
                });

                Ksiazece.app.getController('Push').onPushwooshiOSInitialized(deviceToken);
            },
            function(status) {
                console.warn('failed to register : ' + JSON.stringify(status));
                alert(JSON.stringify(['failed to register ', status]));
            });

        //reset badges on application start
        pushNotification.setApplicationIconBadgeNumber(0);
    },

    initWindowsPushwoosh: function() {
        var pushNotification = window.plugins.pushNotification;

        //set push notifications handler
        document.addEventListener('push-notification', function(event) {
            var title = event.notification.title;
            var userData = event.notification.userdata;

            console.log( event );

            var tabsMap = {
                episode: 0,
                specials: 1,
                ranking: 2,
                generator: 3,
                profile: 4
            };

            if(typeof(userData) != "undefined") {
                console.warn('user data: ' + tabsMap[userData.showView]);

                this.getTabsView().setActiveItem(tabsMap[userData.showView]);
            }
        });

        //initialize the plugin
        pushNotification.onDeviceReady({ appid: "1CA1D-D14FB", serviceName: "" });

        //register for pushes
        pushNotification.registerDevice(
            { appid: "1CA1D-D14FB", serviceName: "" },
            function(status) {
                localStorage.setItem('pushToken', status);

                Ext.data.JsonP.request({
                    url: Ksiazece.Config.getHost() + Ksiazece.Config.getSendPushToken(),
                    params: {
                        accessToken: localStorage.getItem('token'),
                        deviceToken: status
                    },
                    success: function (response, event) {
                        if(response.authorisation == 0) {
                            Ksiazece.app.getController('Account').logout();
                        }
                    },
                    failure: function (response, event) {
                        Ksiazece.app.getController('Main').badConnectionMsg();
                    },
                    scope: this
                });
            },
            function(status) {
                //console.warn(JSON.stringify(['failed to register ', status]));
            }
        );
    }


});