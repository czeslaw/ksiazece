Ext.define('Ksiazece.controller.Main', {
    extend: 'Ext.app.Controller',

    tabsMap: {
        episode: 0,
        specials: 1,
        ranking: 2,
        generator: 3,
        profile: 4
    },

    isMessageShowed: false,

    config: {
        refs: {
            'mainView': 'main',
            'tabsView': 'tabs'
        },
        control: {
            tabsView: {
                activeitemchange: 'onTabChange'
            }
        },
        routes: {
            ':id': 'goToTab'
        }
    },

    alert: function(msg) {
        if (navigator.notification) {
            navigator.notification.alert(msg, Ext.emptyFn, 'Wiadomość', 'Ok');
        } else {
            alert(msg);
        }
    },

    confirm: function(msg, callback) {
        if (navigator.notification) {
            navigator.notification.confirm(msg, callback, 'Wiadomość');
        } else {
            if (window.confirm(msg)) {
                callback();
            }
        }
    },

    backButtonTapped: function(){
        var main = this.getMainView(),
            tabs = this.getTabsView(),
            mainCurrentItem = main.getActiveItem(),
            currentItem;

        if(mainCurrentItem.isXType('login')){
            navigator.app.exitApp();
            console.log( 'zamknij apke' );
        } else {
            currentItem = tabs.getActiveItem();

            if(currentItem.isXType('navigationview')) {
                if(currentItem.getItems().length > 2) {
                    console.log( 'wyjdz wyzej' );
                    currentItem.reset();
                } else {
                    console.log( 'wyloguj' );
                    this.confirm("Czy jesteś pewien, że chcesz się wylogować?", function(){
                        Ksiazece.app.getController('Account').logout();
                    });
                }
            } else {
                console.log( 'wyloguj' );
                this.confirm("Czy jesteś pewien, że chcesz się wylogować?", function(){
                    Ksiazece.app.getController('Account').logout();
                });
            }
        }
    },

    goToTab: function(viewId) {
        if (this.getApplication().application.getHistory().getToken() == viewId) {
            return;
        }

        if (viewId == 'episodes'){
            this.getTabsView().setActiveItem(0);
            delete window.beenHereOnce;
        }
        // this.getTabsView().setActiveItem(this.tabsMap[viewId]);
    },

    onTabChange: function(panel, view, oldView, eOpts) {
        var app = Ksiazece.app,
            viewXtype;

        if (view) {
            viewXtype = view.getXTypes().substr(view.getXTypes().lastIndexOf('/') + 1);

            if (view.isXType('navigationview')) {
                view.reset();
            }

            switch (viewXtype) {
                case 'profile':
                    app.getController('Profile').initView();
                    break;
                case 'specials':
                    app.getController('Specials').initView();
                    break;
            }

            if (typeof window.beenHereOnce == 'undefined'){
                this.redirectTo(viewXtype);
                window.beenHereOnce = true;    
            }
            

            if(viewXtype === 'episodes') {
                viewXtype = 'episode';
            }

            panel.setBadge('', this.tabsMap[viewXtype]);
        }
    },

    /**
     * Pokazuje widok TabPanelu
     * @param animate
     */
    initTabs: function(animate) {
        var app = Ksiazece.app,
            mainView = this.getMainView(),
            view;

        if (localStorage.getItem('userType') == '1') {
            view = Ext.create('Ksiazece.view.generator.Generator');

            setTimeout(function() {
                Ksiazece.app.getController('Generator').initView();
            });
        } else {
            view = Ext.create('Ksiazece.view.Tabs');

            this.updateBadges(view);

            setTimeout(function() {
                app.getController('Episodes').initView();
                //Ksiazece.app.getController('Specials').initView();
                app.getController('Ranking').initView();
                //app.getController('Generator').initView();
                app.getController('Profile').initView(true);
            });
        }

        mainView.add(view);

        if (typeof animate == 'undefined') animate = true;

        if (animate === true) {
            mainView.animateActiveItem(1, {
                type: 'slide',
                direction: 'left'
            });
        } else {
            mainView.setActiveItem(2);
        }
    },

    updateBadges: function(view) {
        var tabsMap = this.tabsMap;

        Ext.data.JsonP.request({
            url: Ksiazece.Config.getHost() + Ksiazece.Config.getBadgesUrl(),
            params: {
                accessToken: localStorage.getItem('token')
            },
            success: function(response, event) {
                var data = response.badges;

                if (response.rules == 0) {
                    Ksiazece.app.getController('Account').logout();
                }

                for (var tab in data) {
                    if(tab == 'profile') {
                        Ksiazece.app.getController('Profile').setInboxBadge(data[tab]);
                    }

                    if (data.hasOwnProperty(tab)) {
                        view.setBadge(data[tab], tabsMap[tab]);
                    }
                }
            },
            failure: function(response, event) {
                this.badConnectionMsg();
            },
            scope: this
        });
    },

    /**
     * Zmienia zakładkę
     * @param viewIndex
     */
    changeTab: function(viewIndex){
        this.getTabsView().setActiveItem(this.tabsMap[viewIndex]);
    },

    /**
     * Wyświetla globalny komuniakt na żółtym tle
     */
    _globalMessage: function(options){
        var message, that = this;

        if(!options.height){
            options.height = (options.text.length/68*50);

            if(options.height < 55) {
                options.height = 55;
            }

            options.height = options.height + 'px';
        }

        if(this.isMessageShowed === false){
            message = Ext.create('Ksiazece.extension.Message', {
                xtype: 'message',
                text: options.text,
                height: options.height,
                hidden: true,
                showAnimation: Ext.os.is.Android ? false : {
                    type: 'slide',
                    direction: 'down',
                    duration: 350
                }
            });

            Ext.Viewport.add(message);
            message.show();

            this.isMessageShowed = true;

            setTimeout(function(){
                message.hide();
                that.isMessageShowed = false;
            }, 5000);
        }
    },

    badConnectionMsg: function(){
        this._globalMessage({
            text: "Problem z połączniem. Spróbuj później.",
            height: '55px'
        });
    }
});