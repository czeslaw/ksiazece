Ext.define('Ksiazece.controller.Generator', {
    extend: 'Ext.app.Controller',

    config: {
        textOnMem: '',
        memRecord: '',

        refs: {
            generatorView: 'generator',
            memList: 'generatorlist',
            showPhotoChooserButton: 'generator button[label=choose]',
            chooseView: 'generatorchoose',
            editView: 'generatoredit',
            editTextArea: 'generatoredit textareafield',
            showEditButton: 'generatorchoose button',
            showListButton: 'generator button[label=showList]',
            photoSelector: 'generatorchoose dataview',
            showPreviewButton: 'generatoredit button',
            previewView: 'generatorpreview',
            changeGeneratedPhoto: 'generatorpreview button[label=change]'
        },
        control: {
            showPhotoChooserButton: {
                tap: 'createNew'
            },
            showEditButton: {
                tap: 'editPhoto'
            },
            showPreviewButton: {
                tap: 'previewPhoto'
            },
            photoSelector: {
                itemsingletap: 'selectPhoto'
            },
            changeGeneratedPhoto: {
                tap: 'backToSelector'
            },
            generatorView: {
                pop: 'onGeneratorPop'
            },
            memList: {
                itemsingletap: 'previewMem'
            },
            showListButton: {
                tap: 'showList'
            },
            editTextArea: {
                keyup: 'countChars'
            }
        }
    },

    /**
     * Liczy znaki i wyświetla limit
     * @param textarea
     * @param e
     * @param eOpts
     */
    countChars: function( textarea,  e, eOpts ) {
        var counterPanel = this.getEditView().query('[label=counter]')[0];

        counterPanel.setData({ charsLeft: 50 - textarea.getValue().length } );
    },

    showList: function(){
        var list = Ext.create('Ksiazece.view.generator.List'),
            store = list.getStore();

        if(Ext.Viewport.getMasked() && Ext.Viewport.getMasked().isHidden()){
            Ext.Viewport.mask({xtype: 'loadmask'});
        }

        store.setParams({
            accessToken: localStorage.getItem('token')
        });

        store.load(function(){
            Ext.Viewport.unmask();
        });

        this.getGeneratorView().add(list);
    },

    previewMem: function(list, index, target, record, e, eOpts){
        var preview = Ext.create('Ksiazece.view.generator.Preview');

        preview.setPreview( record.get('memPicture') );

        this.setMemRecord(record);
        this.setTextOnMem(record.get('text'));

        this.getGeneratorView().add(preview);
    },

    createNew: function() {
        this.setTextOnMem('');
        this.setMemRecord();
        this.choosePhoto();
    },

    onBack: function(){
        var navigation = this.getGeneratorView(),
            view = navigation.getActiveItem();

        if(view.isXType('generatorpreview') && navigation.getInnerItems().length > 3) {
            navigation.reset()
        } else {
            navigation.pop();
        }

        return false;
    },

    onGeneratorPop: function(navigation, view, oldView){
        if(view.isXType('generatorchoose')){
            this.setTextOnMem('');
            this.setMemRecord();
        }
    },

    backToSelector: function(){
        if(this.getGeneratorView().query('generatorchoose').length == 0){
            this.choosePhoto();
        } else {
            this.getGeneratorView().pop('generatorchoose');
        }
    },

    choosePhoto: function(){
        var view = Ext.create('Ksiazece.view.generator.Choose');

        if(this.getMemRecord()) {
            view.setMode('old');
            view.setPreview({
                id: this.getMemRecord().get('pictureID'),
                path: this.getMemRecord().get('picture')
            });
        }

        this.getGeneratorView().push(view);
    },

    selectPhoto: function( dataview, index, target, record, e, eOpts ) {
        this.getChooseView().setPreview(record.getData());
    },

    editPhoto: function(){
        var view = Ext.create('Ksiazece.view.generator.Edit'),
            preview = this.getChooseView().getPreview();

        if( this.getTextOnMem() ){
            view.setMemOnText( this.getTextOnMem() );
        }

        view.setPreview( preview );

        this.getGeneratorView().push(view);
    },

    previewPhoto: function(){
        var editView = this.getEditView(),
            texts = editView.getValues(),
            params = {
                pictureID: editView.getPreview().id,
                accessToken: localStorage.getItem('token')
            };

        if(texts.text1 == ''){
            Ksiazece.app.getController('Main')._globalMessage({ text: "Musisz wpisać tekst", height: '35px'});
            return false;
        } else {
            params.text1 = texts.text1;
            this.setTextOnMem(texts.text1);
        }

        if(this.getMemRecord()) {
            params.memID = this.getMemRecord().get('memID')
        }

        if(Ext.Viewport.getMasked() && Ext.Viewport.getMasked().isHidden()){
            Ext.Viewport.mask({xtype: 'loadmask'});
        }

        Ext.data.JsonP.request({
            url: Ksiazece.Config.getHost() + Ksiazece.Config.getSendMem(),
            params: params,
            success: function (response, event) {
                if(response.authorisation == 0) {
                    Ksiazece.app.getController('Account').logout();
                } else if(response.error){
                    Ksiazece.app.getController('Main')._globalMessage({ text: response.error + response.error+ response.error+ response.error});
                } else {
                    if(this.getMemRecord()){
                        this.getPreviewView().setPreview( response.url );

                        this.getGeneratorView().pop(2);
                    } else {
                        var view = Ext.create('Ksiazece.view.generator.Preview');

                        view.setPreview( response.url );

                        this.getGeneratorView().push(view);
                    }

                    this.setMemRecord(Ext.create('Ksiazece.model.Mem', {
                        memPicture: response.url,
                        memID: response.memID
                    }));
                }

                Ext.Viewport.unmask();
            },
            failure: function (response, event) {
                Ksiazece.app.getController('Main').badConnectionMsg();
                Ext.Viewport.unmask();
            },
            scope: this
        });

        return true;
    }
});
