Ext.define("Ksiazece.Config", {
    singleton: true,

    config: {
        host: 'http://dev.app.4ourclient.com:8080/main/rest-api',//http://192.168.1.63:8888',
        rankingUrl: '/rank',
        rankingLocalUrl: '/rank',
        loginUrl: '/login',
        registerUrl: '/registration',

        citiesListUrl: '/get-cities',
        validateUrl: '/validate',
        acceptRulesUrl: '/accept-rules',
        userDataUrl: '/get-user-data',
        editUserUrl: '/edit-user-data',

	      //messages
	      messageListUrl: '/get-message-list',
				messageUrl: '/get-message',

        //specials
        specialsUrl: '/special-challenge',
        specialSendStep: '/special-send-step',

        //mems
        memImagesUrl: '/get-mem-images',
        memListUrl: '/get-mem-list',
        sendMem: '/send-mem',
        shareMemUrl: '/share-mem',

        //episodes
        episodesListURL: '/weekly-challenge',
        episodeURL: '/get-episode',
        sendEpisodeURL: '/send-episode',
        shareEpisodeUrl: '/share-episode',

        sendAvatar: '/send-avatar',
        sendPushToken: '/send-push-token',

        badgesUrl: '/get-badges',

        sendCityUrl: '/save-city',

        rulesNotAcceptedUrl: '/rules-not-accepted',

        //User data
        birthDate: {},
        //loginData: {},
        //pubData: {},

        quizData: {},

        validation: {}
    },

    constructor: function(config) {
        this.initConfig(config);

        this.callParent([config]);
    }
});