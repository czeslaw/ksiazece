Ext.define('Ksiazece.model.City', {
    extend: 'Ext.data.Model',
    config: {
        fields: ['cityID', 'name']
    }
});