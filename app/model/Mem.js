Ext.define('Ksiazece.model.Mem', {
    extend: 'Ext.data.Model',
    config: {
        fields: ['memPicture', 'memID']
    }
});