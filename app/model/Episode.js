Ext.define('Ksiazece.model.Episode', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
            {
                name: 'title',
                type: 'string'
            },
            {
                name: 'number',
                type: 'int'
            },
            {
                name: 'url',
                type: 'string'
            },
            {
                name: 'episodeID',
                type: 'string'
            },
            {
                name: 'points',
                type: 'int'
            },
            {
                name: 'soon',
                type: 'boolean', defaultValue: false
            },
            {
                name: 'archive',
                type: 'boolean', defaultValue: false
            },
            {
                name: 'header',
                type: 'string',
                convert: function (value, record) {
                    if (value == null) {
                        var number = record.get('number');

                        if(record.get('points')) {
                            return "Odcinek " + number + " <span>Do zdobycia "+record.get('points')+" pkt</span>";
                        } else if (record.get('archive')) {
                            return "Odcinek " + number + " <span>Odcinek archiwalny</span>";
                        } else {
                            return "Odcinek " + number;
                        }
                    }
                    return value;

                }
            }
        ]
    }
});