Ext.define('Ksiazece.model.Message', {
	extend: 'Ext.data.Model',
	config: {
		fields: [
			{
				name: 'text',
				type: 'string'
			},
			{
				name: 'date',
				type: 'date'
			},
			{
				name: 'read',
				type: 'int'
			},
			{
				name: 'messageID',
				type: 'string'
			},
			{
				name: 'title',
				type: 'string'
			},
            {
                name: 'showView',
                type: 'string'
            }
		]
	}
});