<?php

header('Content-Type: text/javascript');

switch($_REQUEST['data']){
	case 'login': 
	case 'register':
		$data = array(
			'status' => 'ok',
 			'accessToken' => 1234567890,
		);
		break;
	case 'ranking':
		$data = array();
		$currentUser = rand(1,50);

		$data['message'] = array(
			'text' => 'Wiadomość z JSONa '. $currentUser,
			'icon' => 'caution'
		);
		$data['data'] = array();

		for ($j = 2; $j > 1; $j--) {
			for ($i = 1; $i <= 10; $i++) {
	 		   array_push($data['data'], array(
					'avatar' =>  './resources/images/avatar-tmp.jpg',
					'name' => 'Mariusz Kiełbasa',
		            'pub' => 'Pub Avangarde 0'.$j,
		            'points' => 1000 - $i,
		            'position' => $i,
		            'month' => '0'.$j.'.2014',
		            'currentUser' =>  ($currentUser == $i)?true:false
				));
			}

			if($currentUser > 10) {
				array_push($data['data'], array(
					'avatar' =>  './resources/images/avatar-tmp.jpg',
					'name' => 'Mariusz Kiełbasa',
		            'pub' => 'Pub Avangarde 0'.$j,
		            'points' => 1000 - $i,
		            'position' => $currentUser,
		            'month' => '0'.$j.'.2014',
		            'currentUser' => true
				));
			}
		}
		break;
	case 'special':
		$specials = array(
			array(
				'rules' => 1,
				'specials' => array(
					'message' => array(
						'text' => 'Umiesz wymyślić dowcip?',
						'icon' => 'joke'
					),
					'type' => 'quiz',
					'description' => 'Jakis opis',
					'data' => array(
						'specialID' => 5,
						'description' => ' tets',
						'steps' => array(
							array(
								'questionID' => 1,
								'question' => '<p>Jak duzo osob?</p>',
								'type' => 2,
								'answers' => array(
									array('answerID' => 1, 'answer' => 'test'),
									array('answerID' => 2, 'answer' => 'test2'),
									array('answerID' => 3, 'answer' =>'test3')
								)
							),
							array(
								'questionID' => 2,
								'question' => 'Jak duzo osob sprzedaje?',
								'type' => 1
							)
						)
					)
				)
			),
			array(
				'rules' => 1,
				'specials' => array(
					'type' => 'joke',
					'message' => array(
						'text' => 'Umiesz wymyślić dowcip?',
						'icon' => 'joke'
					),
					'description' => '<p class="lead">Wiele żartów dotyczy baru, a każdy dobry, kończy się trafną puentą, tak jak rozmowa przez kontuar. Dokończ dowcip i wyślij go nam. Za błyskotliwość i humor możesz dostać nawet 20 punktów. Historia zaczyna się tak:</p>',
					'data' => array(
						'specialID' => 1,
						'question' => '<p>Do baru wchodzi smutny gość z pstrokatą papugą na ramieniu. Barman spogląda pytająco, na co papuga mówi: "Dwa małe poproszę".</p>'.
							'<p>A barman na to&hellip;</p>'
					)
				)			
				
			),
			array(
				'rules' => 1,
				'specials' => array(
					'type' => 'message',
					'message' => array(
						'text' => 'Wyniki bedą za miesiąc',
						'icon' => 'joke'
					)
				)				
			),
			array(
				'rules' => 1,
				'specials' => array(
					'type' => 'results',
					'message' => array(
						'text' => 'Są wyniki',
						'icon' => 'joke'
					),
					'data' => array(
						array(
							'avatar' => "http://dev.app.4ourclient.com:8080/upload/avatars/0/30_m.jpg",
							'name' => "zzzz zzzzq",
							'pub' => "Lokal nowy",
							'points' => "140",
							'position' => "1",
							'month' => "06.2014",
							'currentUser' => false
						),
						array(
							'avatar' => "http://dev.app.4ourclient.com:8080/upload/avatars/0/30_m.jpg",
							'name' => "zzzz zzzzq",
							'pub' => "Lokal nowy",
							'points' => "140",
							'position' => "1",
							'month' => "06.2014",
							'currentUser' => false
						)
					)
				)			
			)			
		);

		$data = $specials[3];
		break;
	case 'send-special': 
		$data = array(
				'rules' => 1,
				'message' => array(
					'text' => 'Umiesz wymyślić dowcip?',
					'icon' => 'joke'
				),
				'specialMessage' => "Odpowiedź została przesłana."
			);
		break;
	case 'cities': 
		$data = array(
			'cities' => array(
					array('cityID' => 123, 'name' => 'Krajenka'),
					array('cityID' => 123, 'name' => 'Kraków'),
					array('cityID' => 123, 'name' => 'Krapkowice'),
					array('cityID' => 123, 'name' => 'Kraśnik'),
					array('cityID' => 123, 'name' => 'Krasnobród'),
					array('cityID' => 123, 'name' => 'Krasnystaw'),
					array('cityID' => 123, 'name' => 'Krobia'),
					array('cityID' => 123, 'name' => 'Krośniewice'),
					array('cityID' => 123, 'name' => 'Krosno'),
					array('cityID' => 123, 'name' => 'Krosno Odrzańskie'),
					array('cityID' => 123, 'name' => 'Kruszwica'),
					array('cityID' => 123, 'name' => 'Krynica Morska'),
					array('cityID' => 123, 'name' => 'Krynica-Zdrój'),
					array('cityID' => 123, 'name' => 'Krynki'),
					array('cityID' => 123, 'name' => 'Krzanowice'),
					array('cityID' => 123, 'name' => 'Krzepice'),
					array('cityID' => 123, 'name' => 'Krzywiń'),
					array('cityID' => 123, 'name' => 'Krzyż Wielkoposlki')
				)
		);
		break;
	case 'userdata':
		$data = array(
			'user' => array(
			'avatar' => "",
			'city' => array(
				'cityID' => 388,
				'name' => "Kraków"
			),
			'userType' => 1,
			'email' => "demo@demo.com",
			'phone' => "12345679",
			'pubname' => "Pub",
			'street' => 'Ulica',
			'streetNo' => 12,
			'surname' => 'nazwisko',
			'name' => 'imie' 
			)
		);
		break;
	case 'episodes':
		if($_REQUEST['page'] == 1) {
			$data = array(
				'rules' => 1,
				'total' => 10,
				'episodes' => array(
					array('title' => 'title', 'episodeID' => 15, 'number' => 10, 'url' => 'resources/images/episode-soon-tmp.jpg'),
					array('title' => 'title', 'episodeID' => 14, 'number' => 9, 'url' => 'resources/images/episode-tmp.jpg', 'points' => 20),
					array('title' => 'title', 'episodeID' => 13, 'number' => 8, 'url' => 'resources/images/episode-tmp.jpg', 'archive' => 1),
					array('title' => 'title', 'episodeID' => 12, 'number' => 7, 'url' => 'resources/images/episode-tmp.jpg', 'archive' => 1),
					array('title' => 'title', 'episodeID' => 11, 'number' => 6, 'url' => 'resources/images/episode-tmp.jpg', 'archive' => 1)
					
				)
			);
		} else {
			$data = array(
				'rules' => 1,
				'total' => 10,
				'episodes' => array(
					array('title' => 'title', 'episodeID' => 5, 'number' => 5, 'url' => 'resources/images/episode-soon-tmp.jpg'),
					array('title' => 'title', 'episodeID' => 4, 'number' => 4, 'url' => 'resources/images/episode-tmp.jpg', 'points' => 20),
					array('title' => 'title', 'episodeID' => 3, 'number' => 3, 'url' => 'resources/images/episode-tmp.jpg', 'archive' => 1),
					array('title' => 'title', 'episodeID' => 2, 'number' => 2, 'url' => 'resources/images/episode-tmp.jpg', 'archive' => 1),
					array('title' => 'title', 'episodeID' => 1, 'number' => 1, 'url' => 'resources/images/episode-tmp.jpg', 'archive' => 1)
					
				)
			);	
		}
		break;
	case 'episode': 
		$data = array(
			'rules' => 1,
			'episode' => array(
				'episodeID' => 1,
				'imageUrl' => 'resources/images/episode-1-tmp.jpg',
				'question' => 'Co ty na to?',
				'answers' => array(
					array('answerID' => 1, 'answer' => 'Racja, ze zboża robi się żytnią, a piwo z szyszek chmielu.'),
					array('answerID' => 2, 'answer' => 'Piwo robi się głównie z ziaren jęczmienia. Chmielu dodaje się bardzo niewiele.'),
					array('answerID' => 3, 'answer' => 'Nieprawda, przecież piwo robi się z prosa. Potem dodaje się liście chmielu.')
				)
			)
		);
		break;
	case 'send-episode': 
		$data = array(
			'rules' => 1,
			'episode' => array(
				'result' => $_REQUEST['answerID'] == 1?1:0, //poprawne: 1, nieporawne: 0
				'points' => 20,
				'message' => array(
					'text' => 'W tym miesiącu grasz o bilety na koncert <b>Coldplay</b>',
					'icon' => 'caution'
				),
				'imageUrl' => 'resources/images/episode-result-tmp.jpg',
			)
		);
		break;	
	case 'get-badges': 
		$data = array(
			'rules' => 1,
			'badges' => array(
		        'episode' => 10,
		        'specials' => 20,
		        'ranking' => 7,
		        'generator' => 4,
		        'profile' => 6	
			)
		);
		break;
}

$callback = $_REQUEST['callback']?$_REQUEST['callback']:'callback';

echo $callback.'('.json_encode($data).')';